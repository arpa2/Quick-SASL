# Quick `n' Easy Authentication with SASL

> *This package "Quick SASL" is useful when adding SASL
> authentication to a protocol.  The extra deliverable
> "Quick DiaSASL" is an even simpler library to pass through
> SASL traffic to a central identity node.*

We developed Quick-SASL as part of
[KIP](http://gitlab.com/arpa2/kip)
but it deserves to be a general package.

The Quick SASL logic is a gentle wrapper around any
SASL implementation, though in practice we have wrapped
it around Cyrus-SASL2, and serve client and server sides
with an easier API.

The Quick DiaSASL logic opens a TCP socket and delegates
SASL packets to and from the central identity node.  It
uses Quick DER to encode and decode the traffic and
adheres to a simple ASN.1 specification.

The protocol with the central identity node is highly
portable due to its design, and it resembles Diameter,
however without adding the full complexity of a
Diameter node.  The idea is that the central identity
node could be a simple Quick SASL service or it could
be a Diameter node.  The latter would be capable of
conducting Realm Crossover, that is, relying on a
remote domain to supply user identity information
underneath its domain.  Diameter is then used to find
and validate the domain part, and considers the remote
authoritative for user identities.  The resulting
identity looks like an email address, is just as
globally identifying and may be learnt from any
domain offering such support for Realm Crossover.
Clients get to Bring Your Own IDentity and do not
need an account on every other machine they run into.

**The home for this software** has moved from the
[KIP package](https://gitlab.com/arpa2/kip) to a
separate [Quick-SASL package](https://gitlab.com/arpa2/Quick-SASL)
after we removed the entanglement with a particular
mechanism that we designed for Realm Crossover,
briefly referred to as SXOVER.


## Using Quick-SASL

Quick-SASL depends on:

  * Cyrus-SASL2 -- the de-facto implementation of SASL
  * [Quick-MEM](https://gitlab.com/arpa2/Quick-MEM) -- a small memory pooling library

To use the `qsasl_` functions, you need an include file or two,

```
#include <arpa2/quick-mem.h>
#include <arpa2/quick-sasl.h>
```

and a library that would base an implementation on a SASL
library, like Cyrus-SASL2, added as

```
-lquicksasl -lquickmem -lsasl2
```

That should be all you need.  The software is small but potent.

The functions are extensively documented in the
include file.


## Using Quick-DiaSASL

Quick-DiaSASL replaces authentication with communication; it depends on:

  * [Quick-DER](https://gitlab.com/arpa2/quick-der) including its `Quick-DERMEM` export -- to send and receive ASN.1 formatted messages
  * [Quick-MEM](https://gitlab.com/arpa2/Quick-MEM) -- a small memory pooling library

To use the `diasasl_` functions, you need a few include files,

```
#include <arpa2/quick-mem.h>
#include <arpa2/quick-diasasl.h>
```

and a library that packs the SASL information with the DiaSASL protocol,

```
-ldiasasl -lquickder -lquickdermem -lquickmem
```

The list is a bit longer, but consists of very small and very
potent code.

The functions are extensively documented in the
include file.


## Authentication Standard

SASL is the official standard for authentication in
IETF protocols.  It was introduced to avoid the returning
requirement of authentication in pretty much all
protocols.  SASL is like a generic pipeline; generic
towards an application protocol that does not have to
know how authentication is done; generic towards
authentication mechanisms that do not need to know
what application protocol uses them.

In essence, SASL passes generic "tokens" from client
to server and back.  Along with the first, a "mechanism"
is selected by its standardised name.  It has not
been standardised how the server communicates the
mechanisms that the client can choose from, but every
protocol contains it in some way.


### Bringing SASL to HTTP

The one exception to the success story is the HTTP
protocol, which prefers to invent its own mechanisms,
and generally does poorly at them.  Several mechanisms
from SASL have been "manually" built into HTTP, that is
such that the application protocol is aware of the
authentication method, and this is precisely where the
web has gone awry.  We are proposing
[SASL for HTTP](https://datatracker.ietf.org/doc/draft-vanrein-httpauth-sasl/)
and even backend support so you can
[Bring Your Own IDentity](https://datatracker.ietf.org/doc/draft-vanrein-diameter-sasl/)
to fix that.  It should also mean that passwords can
stop to occur in the JavaScript space, where the can run
side by side with adverse advertisements.
We have a simple
[demo of HTTP SASL](https://github.com/arpa2/docker-demo/tree/master/demo-ffsasl)
if you are curious.


## Pluggable Backends

There are a few implementations of SASL.  At least in
the open source field we have Cyrus-SASL2, Dovecot
SASL and GNU SASL.  The idea is that the include file
is general (because it aligns with the protocol, which
is the same for all implementations) and the underlying
SASL implementation is chosen by linking to a library
that implements DER SASL on top of one of these
drivers.


## Look-alike Alternative Implementations

The Quick-SASL library API is designed for easy connection
to an application.  The image below shows a number of
alternatives with (almost) the same API, but different
functions.

The Quick-DiaSASL library API is designed for easy
integration into existing server software; it can
accommodate synchronous, threaded or event-looped
operations.  Tokens pass through back and forth
for sessions identified as strings.

We originally designed Quick-SASL as a pluggable API, but
have concluded that there is other pluggability that is
perhaps more potent; notably, some servers already allow
a selection between implementations (library selection in
Postfix; modularly extensible systems like Apache).  And
when we developed our logic for Diameter, we decided that
this was a very good point for routing to a local identity
realm or a remote one.  We present the original idea below,
and mention that Realm Crossover has a highly similar API
to Quick-SASL, but sits higher up in our software stack.
![Variants of Quick SASL allow pluggable servers](doc/sasl-protocol-api.png)
