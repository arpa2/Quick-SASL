# Python packages for KIP and DiaSASL
#
# Author: Rick van Rein <rick.vanrein@arpa2.org>
# SPDX-License-Identifier: BSD-2-Clause


import os
from os import path

import setuptools


#
# Preparation
#
here = path.dirname (path.realpath (__file__))


#
# Extension module for Quick SASL and Xover SASL
#
ext_quicksasl = setuptools.Extension (name='_quicksasl',
			sources=[ path.join (here, 'python', 'quick-sasl', 'quicksasl.i'),
			          path.join (here, 'lib', 'quick-cyrus.c'),
			          path.join (here, 'lib', 'interact.c'),
			],
			swig_opts=[ '-py3', '-I%s' % (path.join (here, 'include'),), ],
			include_dirs=[ path.join (here, 'include'), ],
			libraries=[ 'quickder', 'quickdermem', 'quickmem', 'sasl2', ],
			extra_compile_args=[ '-ggdb3', ],
)



#
# Package Quick SASL
#

readme = open (path.join (here, 'README.MD')).read ()
setuptools.setup (

	# What?
	name = 'quicksasl',
	version = '0.13.2',
	url = 'https://gitlab.com/arpa2/Quick-SASL',
	description = 'Quick SASL -- Pythonic class-based SASL interface -- with SASL Realm Crossover',
	long_description = readme,
	long_description_content_type = 'text/markdown',

	# Who?
	author = 'Rick van Rein (for ARPA2, the SASL Works)',
	author_email = 'rick@openfortress.nl',

	# Where?
	ext_modules = [ ext_quicksasl, ],
	# namespace_packages = [ 'arpa2', ],
	packages = [
		# 'arpa2',
		'quicksasl',
	],
	package_dir = {
		'quicksasl' : path.join (here, 'python', 'quick-sasl'),
	},
	#TODO# package_data = {
	#TODO# 	'quicksasl' : [ path.join (here, 'README.MD'),
	#TODO# 	],
	#TODO# },

	# Requirements
	install_requires = [ 'six' ],
	# extras_require = {
	# 	'TOOLS' : [ 'arpa2.quickder_tools' ],
	# },

)


#
# Package Quick DiaSASL
#

#NOTYET# readme = open (path.join (here, 'DIASASL.MD')).read ()
readme = """
# DiaSASL for Python SASL server

DiaSASL relays server-side SASL interactions to a locally trusted
identity node over plain TCP/IP.  This identity node can be central
to a server site, and address many protocols in a similar way.

Normally, the local identity node relays the traffic to Diameter
and passes it to the service realm or, if a SASL mechanism suited
for Realm Crossover is used, to the client's realm for an identity
that ends in the client's domain name.

**Background information:**

This code can be used in services that want to facilitate
Bring Your Own IDentity to clients, rather than forcing them to
have yet another login under your domain.  Technically, this
requires a mechanism through which users can validate a userid
under their own domain, and your service should (use an identity
node to) validate the client's domain before accepting the
userid underneath it.

This architecture enables clients much more control over their
online identity, both through (service-specific) aliases and
by selecting an authentication mechanisms that fits their needs.
Service providers gain too; they are relieved from the burden
of managing users and their passwords.

Here is a selection of blog articles on this approach:

  * [Bring Your Own IDentity](http://www.internetwide.org/blog/2015/04/22/id-2-byoid.html)
  * [Forms of Identity](http://internetwide.org/blog/2015/04/23/id-3-idforms.html)
  * [Be Your Own IDentity Provider](http://www.internetwide.org/blog/2020/03/18/id-13-idp.html)
  * [Support Levels for Realm Crossover](http://internetwide.org/blog/2020/09/16/id-16-xover-levels.html)
  * [Web Authentication with SASL](http://internetwide.org/blog/2018/11/15/somethings-cooking-4.html)
  * [Bootstrapping Online Identity](http://www.internetwide.org/blog/2020/04/13/id-14-bootstrap.html)

We also publish a Domain Owner's Manual to get users started in
establishing their online freedom and regain first-class
citizenship on the Internet:

  * [Domain Owner's Manual](https://gitlab.com/arpa2/domain-owners-manual)
"""

setuptools.setup (

	# What?
	name = 'quick-diasasl',
	version = '0.13.2',
	url = 'https://gitlab.com/arpa2/Quick-SASL',
	description = 'DiaSASL -- relay SASL server interactions to a site-central identity node',
	long_description = readme,
	long_description_content_type = 'text/markdown',

	# Who?
	author = 'Rick van Rein (for ARPA2, the SASL Works)',
	author_email = 'rick.vanrein@arpa2.org',

	# Where?
	# namespace_packages = [ 'arpa2', ],
	packages = [
		# 'arpa2',
		'quickdiasasl',
	],
	package_dir = {
		'quickdiasasl' : path.join (here, 'python', 'quick-diasasl'),
	},
	#TODO# package_data = {
	#TODO# 	'quickdiasasl' : [ path.join (here, 'README.MD'),
	#TODO# 	],
	#TODO# },

	# Requirements
	install_requires = [ 'six' ],
	# extras_require = {
	# 	'TOOLS' : [ 'arpa2.quickder_tools' ],
	# },

)

