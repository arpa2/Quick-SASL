/** @defgroup quicksasl Quick and Easy SASL
 * @{
 *
 * Quick SASL is a library for embedding SASL in many
 * protocols, including ones based on ASN.1 data structures.
 * It then uses type definitions from Quick DER
 * and integrates easily with that tool.
 *
 * All data is exchanged as a membuf, which is a pair
 * of uint8_t *bufptr and size_t buflen; it is optional
 * data because the bufptr can be set to NULL.
 *
 * SASL tokens are optional byte sequences, so they will
 * mostly be passed by protocols as OCTET STRING OPTIONAL
 * which is literally included in DER, or encoded as hex
 * or base64 strings in JER.  This applies to both the
 * challenges and responses, but also to the s2c token
 * that may travel as a hint with a final acceptance.
 *
 * Mechanisms are limited forms of ASCII strings and are
 * best represented as IA5String values, though other
 * choices are possible.  A list of mechanism names is
 * not standardised, but we can use an IA5String with
 * mechanism names separated by a single space for easy
 * handling.  More complex forms can be had with ASN.1
 * but they hardly add value during processing, and it
 * is beneficial to use one type for Mechanism and the
 * MechanismList.
 *
 * We do not enforce an ASN.1 module, because the types
 * involved are really basic and can be combined with
 * other data as desired.
 *
 * There are a few parameters that the SASL exchange
 * also needs, all of which will be supplied as a
 * membuf, even when they are textual.  This can be
 * helpful when they come from the network protocol.
 *
 * The following contextual items can be set:
 *  - Service Name
 *  - Mechanism List
 *  - Client Realm, Server Realm
 *  - External Client Identity
 *  - Channel Binding
 *
 * The following concluding items can be gotten:
 *  - Authentication Status: UNDECIDED, SUCCESS, FAILURE
 *  - Client Realm, Server Realm (also: Local, Remote)
 *  - Authenticated Client Identity
 *  - Mechanism List (eventually with one entry)
 *
 * The general procedure for Quick SASL is:
 *  - Global Initialisation
 *  - Creation of Client or Server Connection Object
 *  - Setting any Contextual Items
 *  - Moving the Mechanism List from Server to Client
 *  - Moving Tokens between Client and Server
 *  - Retrieving Concluding Items
 *  - Destruction of Connection Object
 *  - Global Cleanup
 *
 * Any user interaction is taken care of behind this
 * API, which is geared towards the protocol engine.
 *
 * There can be various drivers implementing this API.
 * The switch is mostly deferred to package installation
 * time, so the operator has the widest range of choice.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_QUICK_SASL_H
#define ARPA2_QUICK_SASL_H

#include <arpa2/quick-der.h>
#include <arpa2/quick-mem.h>


/** @brief The opaque data for a Quick SASL Connection.
 */
struct qsaslt_connection;
typedef struct qsaslt_connection *QuickSASL;


/** @brief The state of progress of a SASL Connection.
 */
typedef enum qsaslt_state {
	QSA_UNDECIDED = 0,
	QSA_SUCCESS = 1,
	QSA_FAILURE = -1,
} qsaslt_state;


/** @brief Supported types of channel binding.
 *
 */
#define QSA_TLS_UNIQUE "tls-unique"
#define QSA_TLS_SERVER_END_POINT "tls-server-end-point"
#define QSA_X_MANUAL_SESSION_KEY "x-manual-session-key"
#define QSASL_CHANBIND_STRINGS { QSA_TLS_UNIQUE, QSA_TLS_SERVER_END_POINT, QSA_X_MANUAL_SESSION_KEY, NULL }

/** @brief Global Initialisation of Quick SASL.
 *
 * If you need to provide a driver library hint,
 * this is where to do it.  Not actually used yet.
 *
 * The default appname is "Hosted_Identity".
 */
bool qsasl_init (const char *_opt_driver, const char *appname);


/** @brief Global Cleanup of Quick SASL.
 */
void qsasl_fini (void);


/** @brief Create a Client Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_CLIENTFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 *
 * Prior to this call, *out_qsasl must be NULL.
 */
bool qsasl_client (QuickSASL *out_qsasl,
		const char *service,
		void (*opt_cb) (void *cbdata, QuickSASL qsasl, qsaslt_state newstate),
		void *cbdata,
		unsigned flags);


/** @brief Create a Server Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_SERVERFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 *
 * Prior to this call, *out_qsasl must be NULL.
 */
bool qsasl_server (QuickSASL *out_qsasl,
		const char *service,
		void (*opt_cb) (void *cbdata, QuickSASL qsasl, qsaslt_state newstate),
		void *cbdata,
		unsigned flags);

/** @brief The protocol can carry additional data
 * along with a success.
 */
#define QSA_SERVERFLAG_ALLOW_FINAL_S2C 0x0001


/** @brief Close the Client/Server Connection Object.
 *
 * This frees the SASL connection handle, plus
 * any data that was allocated in its pool.
 *
 * After this call, *inout_qsasl will be NULL.
 */
void qsasl_close (QuickSASL *inout_qsasl);


/** @brief Make a step as a SASL Client.
 *
 * The return value is true for success passing through the
 * programming steps, as an accumulated assertion of the entire
 * process.  It does _not_ signify an authentication result.
 *
 * Authentication results are dissiminated through the
 * success and failure flags, and can be requested with
 * qsasl_get_outcome() or through the callback facility.
 *
 * When neither success or failure is set after this step,
 * then authentication needs to continue another step.
 * The two flags should not ever occur together, but it is
 * cautious to treat failure as the leading one when the
 * flow has ended, so when either-or-both are flagged.
 */
bool qsasl_step_client (QuickSASL qsasl,
			membuf s2c,
			membuf *out_mech, membuf *out_c2s);


/** @brief Make a step as a SASL Server.
 *
 * The return value is true for success passing through the
 * programming steps, as an accumulated assertion of the entire
 * process.  It does _not_ signify an authentication result.
 *
 * Authentication results are dissiminated through the
 * success and failure flags, and can be requested with
 * qsasl_get_outcome() or through the callback facility.
 *
 * When neither success or failure is set after this step,
 * then authentication needs to continue another step.
 * The two flags should not ever occur together, but it is
 * cautious to treat failure as the leading one when the
 * flow has ended, so when either-or-both are flagged.
 */
bool qsasl_step_server (QuickSASL qsasl,
			membuf c2s,
			membuf *out_s2c);


/** @brief Determine the flags for success or failure.
 *
 * They may both be reset, in which case the work is not done.
 * The return value indicates success of the call that
 * fills the flags.
 *
 * This interface may seem a bit silly, but it helps to avoid
 * problems that might occur with a ternary output in one integer.
 */
bool qsasl_get_outcome (QuickSASL qsasl, bool *success, bool *failure);


/** @brief Set the Channel Binding
 */
bool qsasl_set_chanbind (QuickSASL qsasl, bool enforce, membuf value);


/** @brief Get the Channel Binding value.
 */
bool qsasl_get_chanbind_value (QuickSASL qsasl, membuf *value);


/** @brief Get the Channel Binding name.
 */
bool qsasl_get_chanbind_name (QuickSASL qsasl, const char **name);


/** @brief Set the Server Realm.
 */
bool qsasl_set_server_realm (QuickSASL qsasl, membuf srealm);


/** @brief Set the Client Realm.
 */
bool qsasl_set_client_realm (QuickSASL qsasl, membuf crealm);


/** @brief Get the Mechanism(s) that is/are currently possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool qsasl_get_mech (QuickSASL qsasl, membuf *out_mech);


/** @brief Set the Mechanism(s) to be considered possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool qsasl_set_mech (QuickSASL qsasl, membuf mech);


/** @brief Set the Client Identity (for SASL EXTERNAL).
 */
bool qsasl_set_clientid_external (QuickSASL qsasl, membuf clientid);


/** @brief Set the Client username for authentication (the "login user").
 */
bool qsasl_set_clientuser_login (QuickSASL qsasl, membuf user_login);


/** @brief Set the Client username to act for (the "ACL user").
 */
bool qsasl_set_clientuser_acl (QuickSASL qsasl, membuf user_acl);


/** @brief Get the Client UserID, available after success.
 *
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return derptr==NULL,
 * because no client identity has been established.
 */
bool qsasl_get_client_userid (QuickSASL qsasl, membuf *out_userid);


/** @brief Get the Client Domain, available after success.
 *
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return the approving realm.
 * even though no client UserID has been established.
 */
bool qsasl_get_client_domain (QuickSASL qsasl, membuf *out_domain);


/** @brief Retrieve a delegated credential (a "token" of some kind).
 *
 * Some mechanisms may delegate a credential to the
 * client.  This can be retrieved with this call.
 * The call sets DER NULL if no credential was
 * presented, but still returns true.
 */
bool qsasl_get_delegated_credential (QuickSASL qsasl, membuf *out_cred);


/** @brief Get an informal client hint string, usually after success.
 *
 * This may be used to trace them, possibly even shown on
 * their interface, though no formal meaning should ever
 * be assigned to it, as would be possible with _clientid().
 * This is meant to return the unreliable information that
 * the ANONYMOUS mechanism shares.  Other sources of this
 * information may also be used, as long as privacy of
 * other users is not hampered.  Expect to see derptr==NULL
 * if nothing is available.
 */
//NOTIMPL// bool qsasl_get_clienthint (QuickSASL qsasl, membuf *out_clientid);



/********** ASSORTED UTILITY FUNCTIONS **********/



/** @brief Allocate buffer memory.
 *
 * This memory pool will be held on to until
 * the qsasl_close() for the QuickSASL connection.
 * The memory is zeroed before it is returned.
 *
 * When this fails, it sets errno to ENOMEM.
 *
 * Since QuickSASL allocates a fresh mem_pool,
 * this is just a wrapper around mem_alloc().
 */
static inline bool qsasl_alloc (QuickSASL qsasl, size_t size, uint8_t **out_memptr) {
	return mem_alloc ((void *) qsasl, size, (void **) out_memptr);
}


/** @brief Buffer a character string provided as a membuf.
 *
 * The character string need not have a trailing NUL char,
 * but it will be made available in the master's pool with
 * a trailing NUL.
 *
 * Before this call, *out_strbuf must be NULL.
 */
static inline bool qsasl_strdup (QuickSASL qsasl, membuf str, char **out_strbuf) {
	return mem_strdup ((void *) qsasl, str, out_strbuf);
}


/** @brief Test if support for Quick SASL passphrase entry is
 * available.
 *
 * This may check for existence of a pinentry program in the
 * local setup.
 */
bool qsasl_haveinteraction (void);


/** @brief First usage pattern: passphrase entry.
 *
 * The return value * is NULL on failure (with errno set) or
 * a NUL-terminated string which must be cleared and free()d when done.
 *
 * SETTIMEOUT 30
 * SETDESC Passphrase for https://github.com Realm ARPA2
 * SETPROMPT [user@]pass
 * SETTITLE Login with HTTP-SASL
 * SETKEYINFO s/AABBCCDD
 * SETOK Login
 * SETCANCEL Anonymous
 * GETPIN
 *
 * The parameters should be provided by the context, in
 * desc_for, prompt, title.  The desc_for is printed after
 * "Passphrase for " and since this value is expected to
 * identify the resource to login to, it is turned to hex
 * for use in SETKEYINFO, which may be used to store the
 * key in a system-specific password storage mechanism.
 *
 * The text for Login and Cancel buttons may be replaced.
 * A useful idea is to replace Cancel with something
 * positive, like "Anonymous".  To facilitate this, the
 * Cancel operation returns a fixed string that must not
 * be free()d, holding the pointer fixptr_cancel.
 */
char *qsasl_getpassphrase (char *title, char *desc_for, const char *prompt, char *login, char *cancel, char *fixptr_cancel);


/** @brief Second usage pattern: Ask for Confirmation.
 *
 * SETTIMEOUT 30
 * SETDESC Login as vanrein@github.com
 * SETTITLE Approval for Client Identity
 * SETOK OK
 * SETNOTOK Change
 * SETCANCEL Cancel
 * CONFIRM
 *
 * The desc and title values should be supplied.  Alternate
 * texts for OK and Cancel buttons may be given.  The return
 * value is:
 *  1 for OK
 *  0 for NOTOK
 * -1 for CANCEL or TIMEOUT
 */
int qsasl_confirm (char *title, char *desc, char *ok, char *notok, char *cancel);



/***** ENVIRONMENT VARIABLES *****/


/** @brief Environment variable to hold passphrases.
 *
 * Yay, we invented indirections in bash variables.
 * Now you go and figure out ${!X} or use proper C.
 */
#define QSASL_PASS_ENVVAR "QUICKSASL_PASSPHRASE"


#endif  /* ARPA2_QUICK_SASL_H */

/** @} */
