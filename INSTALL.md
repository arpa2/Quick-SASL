<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Build and Install Instructions

> These are instructions for building and installing ARPA2 components
> by hand. If your operating system has packages available for the
> components, you are advised to use those.

## In A Nutshell

Like all ARPA2 (sub-)projects, Quick SASL has a three-stage build:
 - run `cmake` with suitable arguments
 - run the build-system that is generated
 - run the install step that is generated

For Windows, this is probably different (e.g. all contained within Visual
Studio. On UNIX-like systems, to spell it out,

```
mkdir build
cd build
cmake ..
make
make install
```

This creates a separate build directory (inside the source checkout; you
can also do it somewhere else entirely), runs `cmake` telling it where
the sources are. The default generator for CMake is Makefiles, so
then `make` is the tool to do the build and install steps.


## Dependencies

Quick SASL uses [CMake](https://cmake.org/) to build; you will need at least
version 3.18. When running cmake, missing dependencies are reported.

### Required

The following ARPA2 software is needed to build Quick SASL.
These are listed in recommended dependency order, if you are
building the entire stack.
 - *ARPA2CM*, the cmake-support modules for the ARPA2 stack.
 - *arpa2common*, basic libraries and development convenience. These are
   strictly required only if the `DEBUG` option is on. When the option
   is off, no files from *arpa2common* are used.
 - *Quick MEM*, memory pool allocator.
 - *Quick DER*, LDAP-data (DER format) decoding.

The following third-party software is required when building Quick SASL:
 - *Cyrus SASL*
   Debian package: libsasl2-dev
   FreeBSD package:
   openSUSE package:
 - *com_err*
   Debian package:
   FreeBSD package:
   openSUSE package:

### Recommended

The following third-party software is recommended when building Quick SASL,
for the full range of features and documentation:
 - *Doxygen* produces browsable documentation (also needs dot, from graphviz).
   Debian package: doxygen graphviz
   FreeBSD package: devel/doxygen
   openSUSE package:
 - *Threads* enables threaded examples
 - *LibEv* enables event-loop examples

## Options

Quick SASL has the following options:
- *CUT_THROAT_TESTING* is present but has no effect at all.
- *DEBUG* Sets compilation flags suitable for debugging the project.
- *NO_DAEMONS* Do not build daemons and server management programs.

The software -- libraries and executables -- is installed in the prefix defined
by CMake. Use `-DCMAKE_INSTALL_PREFIX` at cmake-time to install to a
different prefix.
