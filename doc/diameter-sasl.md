# SASL Realm Crossover with Diameter

> *Realm Crossover enables a client to Bring Your Own IDentity (BYOID) to
> a foreign server, even during first contact, and allow the foreign server
> to validate their identity by calling back the the identity provider
> in the client realm.*

Two authentication mechanisms are common in protocols, namely SASL and Kerberos.
Both can be made to work with Realm Crossover.  For SASL, the approach is to
construct an end-to-end encryption tunnel between the client and their
realm, and login over that tunnel.

## SXOVER is a SASL Mechanism for Realm Crossover

The foreign server sits in between passing tunnel messages, but it does pickup 
when the client realm reports that it authanticated with a certain userid.
Since the foreign server can validate that the tunnel was made to the client's
realm, it can now combine the userid with the realm to form the client's full
`user@domain.name` identity.

The client may negotiate the delivery of an alias or pseudonym, of course.
This would be how it wants to appear to this foreign server.  This is a
model that provides reasonable privacy (there need not be an email address
that looks like this `user@domain.name`, for example) but it is much more
useful to the server than anonymity, where it cannot even distinguish whether
two client requests are made by the same client when this would be the client's
intention.

## Reliance on KIP for Tunnel Encryption

The KIP library is used to encrypt the tunnel between the client and its realm.
Though defined as general encryption with Kerberos mechanisms, the KIP library
is perfectly suited to provide just that.

## Reliance on Diameter for the Realm Backlink

The foreign server connects to the client realm via a standardised authentication
protocol.  It is explicitly required that this protocol offers no resources (like
an email account would) but basically answers *aye* or *nay* to an authentication
request.  This is done with Diameter.  If you know RADIUS, you pretty much know
Diameter, with the difference that it was designed to securely scale up to the
work over a large, potentially evil network like the Internet.

Diameter runs over SCTP (to avoid head-of-line blocking) which you should be able
to find on all server platforms.  It also uses D/TLS to protect the connection,
with a requirement that both ends validate their identity.  This means that the
client realm is aware of the party that is asking for a clientid.  This can be
helpful in establishing an alias.

## Server-side Components

The server side of Realm Crossover for SASL is a freeDiameter server with the
`diasasl.fdx` extension built herein.  The fragment to configure the server in
a file like `/etc/freeDiameter/freeDiameter.conf` would look like

```
# -------- example.com, idp at host id0 ---------

Identity = "id0.example.com";
Realm = "example.com";

# -------- SCTP over IPv6 ---------

No_TCP;
No_IP;
SCTP_Streams = 10;

# ListenOn = "2001:db8:123:456::99";
ListenOn = "::1";
Port = 3868;
SecPort = 3869;

# --------- Certificates for D/TLS ---------

TLS_Cred = "diacrt.pem", "diakey.pem";
TLS_CA   = "ca.pem";
TLS_DH_File = "dh.pem";

# --------- ARPA2 extension, the GS2-SXOVER-PLUS protocol ---------

LoadExtension = "diasasl.fdx" : "/etc/freeDiameter/diasasl-extension-server.conf";

# --------- If so desired, link directly to known peers ---------
```

Note the reference in the last configuration line to a `diasasl-server.conf`
that reads

```
vendor-id=44469;
appli-id=1;
cmd-id=265;
mode=1;
```

This specifies ARPA2 as a vendor identity and the Network Access Server the application ID.

With these files, you can run the `freeDiameterd` program, which loads the `diasasl.fdx` extensions and opens up to the World.


## Client Access

Clients need to install the SASL contrib for the `GS2-SXOVER-PLUS` mechanism, or
SXOVER for short.  This allows them to select the tunnel when it is offered by a
foreign server.  It is safe to run this server towards *any foreign server* due to
the `-PLUS` part that involves channel binding to an underlying secure layer, such
as TLS or perhaps TCPcrypt in the future.

Within the tunnel, a nested SASL interaction is started, which can be any of the
classical choices.  This includes Kerberos, so there already is support for the
use of this authentication mechanism; it is desirable because it is used with a
single login per day and works over virtually all protocols; the so-called
Single Sign-On advantage.  Before you have Kerberos setup however, you may want
to use a simpler password-based mechanism, like `SCRAM-SHA256`.


## Uplink for the Foreign Server

A foreign server that wants to support Realm Crossover with SASL may do so by
linking in our `libxoversasl.so` library.  This library encapsulates the parts of
a SASL query that matter into an ASN.1 specified and DER encoded message, and
sends that to the freeDiameter daemon.

The protocol used for this assumes a trusted network, so the protocol is only
intended for internal use.  The idea is that the freeDiameter daemon, which
generally serves as a peer on the authentication network, offers an uplink
onto the peering network formed by the SXOVER mechanism.

It is perfectly possible to setup a local realm on the same freeDiameter node.
In that case, the uplink protocol can be used to access the local realm
directly, with accounts managed centrally and passwords never shared to the
foreign servers.

When used with clients from foreign realms, the only mechanism to use should
be SXOVER; this is generally the client's choice because it is the only one
who knows what identity it want to use.  When used with clients accessing
local realms, it is additionally possible to use other SASL mechanisms, because
their traffic does not leave the internal network.  Do note however, that
the traffic may still travel over unencrypted TCP between the foreign server
and the freeDiameter node.

