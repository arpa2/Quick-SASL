/* Test the xoverpass_info() function. */


#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>


#include <arpa2/quick-diasasl.h>


bool test (char *mech, char *token, uint32_t tokenlen,
			char *realm, char *cbtyp, bool fit4plain) {
	char *got_realm; uint32_t got_realmlen;
	char *got_cbtyp; uint32_t got_cbtyplen;
	bool got_fit4plain;

	diasasl_mechinfo (mech, (uint8_t *) token, tokenlen,
			&got_realm, &got_realmlen,
			&got_cbtyp, &got_cbtyplen,
			&got_fit4plain);

	bool ok = true;

	if (((realm == NULL) && (got_realm != NULL)) || (got_realmlen != strlen (realm?realm:"")) || strncmp (got_realm, realm, got_realmlen)) {
		printf ("Realm difference: \"%.*s\" != \"%s\"\n",
			got_realmlen, got_realm, realm);
		ok = false;
	}
	if (((cbtyp == NULL) && (got_cbtyp != NULL)) || (got_cbtyplen != strlen (cbtyp?cbtyp:"")) || (strncmp (got_cbtyp, cbtyp, got_cbtyplen))) {
		printf ("Channel Binding Type difference: \"%.*s\" != \"%s\"\n",
			got_cbtyplen, got_cbtyp, cbtyp);
		ok = false;
	}
	if (got_fit4plain != fit4plain) {
		printf ("Fit4PLAIN difference: %s != %s\n",
			got_fit4plain ? "true" : "false",
			fit4plain     ? "true" : "false");
		ok = false;
	}

	return ok;
}

int main (int argc, char *argv []) {
	assert (test ("DIGEST-MD5",
			NULL, 0,
			NULL, NULL, false));
	assert (test ("DIGEST-MD5",
			"rauth=123", 0,
			NULL, NULL, false));
	assert (test ("SXOVER-PLUS",
			"p=tls-unique,,orvelte.nep,asdlkjaldksajd", 40,
			"orvelte.nep", "tls-unique", true));
	assert (test ("SXOVER-PLUS",
			"p=tls-unique,a=ikke,orvelte.nep,asdlkjaldksajd", 40,
			NULL, NULL, true));
	assert (test ("SXOVER-PLUS",
			"y=tls-unique,,orvelte.nep,asdlkjaldksajd", 40,
			NULL, NULL, true));
	assert (test ("SXOVER-PLUS",
			"n,,orvelte.nep,asdlkjaldksajd", 40,
			NULL, NULL, true));
	assert (test ("SXOVER-PLUS",
			"p=tls-unique,,orvelte.nep.asdlkjaldksajd", 40,
			NULL, NULL, true));
}
