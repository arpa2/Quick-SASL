/* Quick-DiaSASL server for the API.
 *
 * This program takes in SASL requests and directs them to Quick SASL.
 * It may be used on networks where the facilitation of Realm Crossover
 * through Diameter is not desired, but centralisation of identity onto
 * a central node is.
 *
 * Note that it is usually a better idea to proceed all the way, and
 * actually implement the Diameter variant.  This program is initially
 * designed to allow testing of Quick-DiaSASL without Diameter, but it
 * may be updated to allow more generaly use.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <sys/random.h>
#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/quick-sasl.h>
#include <arpa2/quick-diasasl.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-der.h>
#include <quick-der/Quick-DiaSASL.h>

typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Request DER_Request;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Open_Answer DER_OpenAnswer;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Authn_Answer DER_AuthnAnswer;


#include "sasl-common.c"



/* Receive a request.
 */
bool recv_request (void *master, int cnx, DER_Request *req) {
	static const derwalk pack_der_request [] = {
		DER_PACK_Quick_DiaSASL_DiaSASL_Request,
		DER_PACK_END
	};
	return dermem_recv (master, cnx,
			pack_der_request, (struct dercursor *) req);
}



/* Main program.
 */
int main (int argc, char *argv []) {
	//
	// Harvest commandline arguments
	assert (argc > 0);
	if ((argc < 3) || (argc > 4)) {
		fprintf (stderr, "Usage: %s serverIP serverPort [chanbindvalue]\n",
				argv [0]);
		exit (1);
	}
	socket_init();
	const char *service = "diasasl-demo";
	char *server_ip   = argv [1];
	char *server_port = argv [2];
	char *chan_bind   = (argc >= 4) ? argv [3] : NULL;
	//
	// Harvest environment variables
	char *str_realm = NULL;  /* getenv ("SASL_CLIENT_REALM"); */
	char *str_servr = getenv ("SASL_REALM");
	if (str_servr == NULL) {
		fprintf (stderr, "Please set environment variable SASL_REALM\n");
		exit (1);
	}
	//
	// Have streams for networking
	int sox = -1;
	struct sockaddr_storage ss;
	memset (&ss, 0, sizeof (ss));
	if (!socket_parse (server_ip, server_port, &ss)) {
		fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
		exit (1);
	}
	if (!socket_server (&ss, SOCK_STREAM, &sox)) {
		perror ("Failed to serve network");
		exit (1);
	}
	printf ("--\n");
	fflush (stdout);
	int cnx = accept (sox, NULL, NULL);
	if (cnx < 0) {
		perror ("Failed to accept connection");
		exit (1);
	}
	//
	// Start the SASL session as a server.
	qsasl_init (NULL, "Hosted_Identity");
	QuickSASL sasl = NULL;
	qsaslt_state curstate = QSA_UNDECIDED;
	assert (qsasl_server (&sasl, service, cb_statechange, &curstate, QSA_SERVERFLAG_ALLOW_FINAL_S2C));
	//
	// Pickup the DiaSASL-Open-Request
	DER_Request req0;
	assert (recv_request (sasl, cnx, &req0));
	assert (!der_isnull (&req0.open_request.service_realm));
	//
	// Be someone -- including step down to the ACL user
	assert (dermem_strdup (sasl, req0.open_request.service_realm, &str_realm));
	log_debug ("Received a DiaSASL-Open-Request for realm \"%s\"", str_realm);
	membuf buf_realm = { .bufptr=(uint8_t*)str_realm, .buflen=strlen (str_realm) };
	membuf buf_servr = { .bufptr=(uint8_t*)str_servr, .buflen=strlen (str_servr) };
	assert (qsasl_set_client_realm     (sasl, buf_realm));
	assert (qsasl_set_server_realm     (sasl, buf_servr));
	//
	// Setup a session-id
	uint8_t sesidbytes [16];
	char sesidstring[33];
	assert (getrandom (sesidbytes, 16, 0) == 16);
	sprintf (sesidstring, "%lx%lx%lx", *(uint32_t*)(sesidbytes+0), *(uint32_t*)(sesidbytes+4), *(uint32_t*)(sesidbytes+8), *(uint32_t*)(sesidbytes+12));
	dercursor sesid = { .derptr = sesidstring, .derlen = 32 };
//TODO// CHANBIND COMES FROM FIRST AUTHN-REQUEST
//TODO// USE A PROPER CHANBIND TYPE NAME
	if (chan_bind != NULL) {
		uint8_t *cb = NULL;
		assertxt (qsasl_alloc (sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, &cb), "Failed to allocate channel binding value");
		sprintf ((char*)cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
		membuf mem_chanbind = {
			.bufptr = cb,
			.buflen = strlen ((char*)cb)
		};
		assert (qsasl_set_chanbind (sasl, false, mem_chanbind));
	}
	//
	// Now provide a list of mechanisms for the client
	// Mechanism lists are not standardised but these demos pass them as
	// a string with spaces between the standardised mechanism names.
	dercursor mechs;
	assert (qsasl_get_mech (sasl, crs2bufp (&mechs)));
	log_debug ("Sending mechanism list \"%.*s\"", (int) mechs.derlen, mechs.derptr);
	//
	// Send back the DiaSASL-Open-Answer
	static const derwalk pack_open_answer [] = {
		DER_PACK_Quick_DiaSASL_DiaSASL_Open_Answer,
		DER_PACK_END
	};
	static const derwalk pack_authn_answer [] = {
		DER_PACK_Quick_DiaSASL_DiaSASL_Authn_Answer,
		DER_PACK_END
	};
	DER_OpenAnswer ans0;
	memset (&ans0, 0, sizeof (ans0));
	ans0.service_realm = req0.open_request.service_realm;
	ans0.session_id = sesid;
	ans0.sasl_mechanisms = mechs;
	assert (dermem_send (sasl, cnx, pack_open_answer, (struct dercursor *) &ans0));
	//
	// Await the response from the client: mech, [cb], [token]
	DER_Request req1;
	DER_AuthnAnswer ans1;
	do {
		if (!recv_request (sasl, cnx, &req1)) {
			fprintf (stderr, "Failed to receive a Quick-DiaSASL message; bailing out\n");
			close (cnx);
			close (sox);
			// sleep (1);
			exit (1);
		}
	} while (!der_isnull (&req1.close_request.session_id));
	assert (!der_isnull (&req1.authn_request.session_id));
	membuf mech_used = crs2buf (req1.authn_request.sasl_mechanism);
	assert (!mem_isnull (&mech_used));
	assert (qsasl_set_mech (sasl, mech_used));
	log_debug ("First DiaSASL-Authn-Request, mechsel \"%.*s\"",
				(int) mech_used.buflen, mech_used.bufptr);
	membuf s2c;
	//
	// Start looping as a server
	while (curstate == QSA_UNDECIDED) {
		//
		// Receive a response from the client
		membuf c2s = crs2buf (req1.authn_request.sasl_token);
		//
		// Make a server step: c2s -> s2c
		assert (qsasl_step_server (sasl, c2s, &s2c));
		memset (&ans1, 0, sizeof (ans1));
		ans1.sasl_token = buf2crs (s2c);
		ans1.session_id = req1.authn_request.session_id;
		//
		// Do not send a new challenge when the last response was final
		if (curstate != QSA_UNDECIDED) {
			break;
		}
		//
		// Send output to the client; initially "mech> " and later "s2c> "
		assert (dermem_send (sasl, cnx, pack_authn_answer, (struct dercursor *) &ans1));
		assert (recv_request (sasl, cnx, &req1));
		if (!der_isnull (&req1.close_request.session_id)) {
			fprintf (stderr, "Unexpected DiaSASL-Close-Request\n");
			exit (1);
		}
		assert (!der_isnull (&req1.authn_request.session_id));
		log_debug ("Received a follow-up DiaSASL-Authn-Request");
	}
	//
	// We may still send the "extra>" data in our final response
	int32_t final_comerr_value;
	der_buf_int32_t final_comerr_store;
	if (curstate == QSA_SUCCESS) {
		final_comerr_value = 0;
	} else {
		//TODO// Authentication failure, get a com_err code
		final_comerr_value = 666;
		memset (&ans1.sasl_token, 0, sizeof (ans1.sasl_token));
	}
	ans1.final_comerr = der_put_int32 (final_comerr_store, final_comerr_value);
	//
	// Determine the mechanism used, if it was one, and the clientid
	membuf clientuid;
	if (qsasl_get_client_userid (sasl, &clientuid)) {
		if (clientuid.bufptr == NULL) {
			printf ("No client userid\n");
		} else {
			printf ("Client userid is %.*s\n", (int) clientuid.buflen, clientuid.bufptr);
			ans1.client_userid = buf2crs (clientuid);
		}
	}
	membuf clientdom;
	if (qsasl_get_client_domain (sasl, &clientdom)) {
		if (clientdom.bufptr == NULL) {
			printf ("No client domain\n");
		} else {
			printf ("Client domain is %.*s\n", (int) clientdom.buflen, clientdom.bufptr);
			ans1.client_domain = buf2crs (clientdom);
		}
	}
	if (mech_used.bufptr != NULL) {
		printf ("Mechanism used was %.*s\n", (int) mech_used.buflen, mech_used.bufptr);
	}
	//
	// Send the final verdict to the client
	assert (dermem_send (sasl, cnx, pack_authn_answer, (struct dercursor *) &ans1));
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s the client.\n*\n\n",
			(curstate == QSA_SUCCESS) ? "SUCCESS" : "FAILURE",
			(s2c.bufptr == NULL) ? "should tell" : "has told");
	//
	// Wait for the client to close the connection
	if (cnx >= 0) {
		close (cnx);
	}
	if (sox >= 0) {
		close (sox);
	}
	//
	// Cleanup & Closedown
	qsasl_close (&sasl);
	qsasl_fini ();
	exit ((curstate == QSA_SUCCESS) ? 0 : 1);
}
