/*
 * Author: Henri Manson <info@mansoft.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <locale.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <arpa2/quick-der.h>
#include <arpa2/socket.h>

#include <quick-der/Quick-DiaSASL.h>
#include <arpa2/quick-diasasl.h>

#include "sasl-common.c"

#define DEFAULT_PORT "16832"

static char *conffile = NULL;

static char *program;

static diasasl_node_t dianode;

struct diasasl_data {
	struct diasasl_session diasasl;
	QuickSASL sasl;
	membuf mechlist;
	membuf s2c_token;
	bool success;
	bool failure;
};

/* Callback from diasasl.
 */
void diasasl_cb (struct diasasl_session *session,
		const int32_t *com_errno,
		char *opt_mechanism_list,
		uint8_t *s2c_token, uint32_t s2c_token_len) {
	struct diasasl_data *s = (struct diasasl_data *) session;
	assert (!s->success);
	assert (!s->failure);
	s->s2c_token.bufptr = NULL;
	s->s2c_token.buflen = 0;
	if (com_errno == NULL ? false : *com_errno != 0) {
		s->failure = true;
		com_err (program, *com_errno, "Diameter SASL failure");
		diasasl_close (&dianode, session);	//TODO//FIND_AS_LINK//
	} else if (opt_mechanism_list != NULL) {
		assert (mem_isnull (&s->mechlist));
		assert (mem_isnull (&s->s2c_token));
		s->mechlist.bufptr = (uint8_t *)strdup (opt_mechanism_list);
		s->mechlist.buflen = strlen (opt_mechanism_list);
	} else if (s2c_token != NULL) {
		assert (mem_alloc (s, s2c_token_len, (void **) &s->s2c_token.bufptr));
		memcpy (s->s2c_token.bufptr, s2c_token, s2c_token_len);
		s->s2c_token.buflen = s2c_token_len;
	}
}

static void der_client(char *server_ip, char *server_port, char *chan_bind)
{
	struct sockaddr_storage ss;

	memset (&ss, 0, sizeof (ss));
	if (!socket_parse (server_ip, server_port, &ss)) {
		fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
		exit (1);
	}
	int sox;
	if (!socket_client (&ss, SOCK_STREAM, &sox)) {
		perror ("Failed to connect");
		exit (1);
	}

	memset(&dianode, 0, sizeof(dianode));
	dianode.socket = sox;
	//
	// Start the SASL session as a client.
	diasasl_node_open (&dianode);
	qsasl_init (NULL, "Hosted_Identity");
	//
	// Be someone -- including step down to the ACL user
	char *str_login = getenv ("SASL_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("SASL_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("SASL_CLIENT_REALM"    );
	char *str_servr = getenv ("SASL_REALM"           );
	char *service   =         "diasasl-demo"          ;
	assert(service != NULL);

	membuf crs_login = { .bufptr=(uint8_t *)str_login, .buflen=strlen (str_login) };
	membuf crs_acl   = { .bufptr=(uint8_t *)str_acl  , .buflen=strlen (str_acl  ) };
	membuf crs_realm = { .bufptr=(uint8_t *)str_realm, .buflen=strlen (str_realm) };
	membuf crs_servr = { .bufptr=(uint8_t *)str_servr, .buflen=strlen (str_servr) };
	printf("Creating new sasl\n");
	fflush(stderr);
	//
	// Start the SASL session as a client.
	struct diasasl_data *sasl_data = NULL;
	assert (mem_open (sizeof (*sasl_data), (void **) &sasl_data));

	assert (qsasl_client (&sasl_data->sasl, service, NULL, NULL, 0));
	assert (qsasl_set_clientuser_login (sasl_data->sasl, crs_login));
	assert (qsasl_set_clientuser_acl   (sasl_data->sasl, crs_acl  ));
	assert (qsasl_set_client_realm     (sasl_data->sasl, crs_realm));
	assert (qsasl_set_server_realm     (sasl_data->sasl, crs_servr));
	membuf crs_chanbind;
	membuf mem_chanbind;
	if (chan_bind != NULL) {
		uint8_t *cb = NULL;
		assertxt (qsasl_alloc (sasl_data->sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, &cb), "Failed to allocate channel binding value");
		sprintf ((char *)cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
		mem_chanbind.bufptr = cb;
		mem_chanbind.buflen = strlen ((char *)cb);
		assert (qsasl_set_chanbind (sasl_data->sasl, false, mem_chanbind));
	} else {
		mem_chanbind.bufptr = NULL;
		mem_chanbind.buflen = 0;
	}


	bool success = false;
	bool failure = false;

	sasl_data->diasasl.callback = diasasl_cb;
	diasasl_open (&dianode, &sasl_data->diasasl, "unicorn.demo.arpa2.org", NULL);	/* Server domain */
	assert (diasasl_process (&dianode, true) == 0);

	assert(sasl_data->mechlist.bufptr != NULL);
	assert(qsasl_set_mech(sasl_data->sasl, sasl_data->mechlist));
	for (;;) {
		membuf mech;
		membuf c2s;

		assert(qsasl_step_client(sasl_data->sasl, sasl_data->s2c_token, &mech, &c2s));

		assert(qsasl_get_outcome(sasl_data->sasl, &success, &failure));
		printf("qsasl_get_outcome, success: %d, failure: %d\n", success, failure);
		if (success || failure) {
			break;
		}

		char *opt_mech_choice = NULL;
		sasl_data->diasasl.chanbind     = NULL;
		sasl_data->diasasl.chanbind_len = 0;
		if (mech.bufptr != NULL) {
			mem_strdup (sasl_data, mech, &opt_mech_choice);
			printf("chosen mechanism: %s\n", opt_mech_choice);

			if (mem_chanbind.bufptr != NULL) {
				printf("setting channel binding: %s\n", chan_bind);
				sasl_data->diasasl.chanbind     = mem_chanbind.bufptr;
				sasl_data->diasasl.chanbind_len = mem_chanbind.buflen;
			}
		}

		diasasl_send (&dianode, &sasl_data->diasasl, opt_mech_choice, c2s.bufptr, c2s.buflen);
		assert (diasasl_process (&dianode, true) == 0);
	}
	qsasl_close(&sasl_data->sasl);
	qsasl_fini ();
	diasasl_node_close (&dianode);
	printf("DONE!\n");
	printf("--\n");
	mem_close ((void**) &sasl_data);
	sleep(1);
	socket_close(sox);
}

int main(int argc, char *argv[])
{
	//
	// Harvest commandline arguments
	program = argv [0];
	if (argc >= 2)
	{
		char *server_ip   = (argc >= 2) ? argv [1] : NULL        ;
		char *server_port = (argc >= 3) ? argv [2] : DEFAULT_PORT;
		char *chan_bind = (argc >= 4) ? argv [3] : NULL;

		der_client(server_ip, server_port, chan_bind);
	} else {
		fprintf(stderr, "Usage: %s <IP> [port [chanbindvalue]]\n", argv[0]);
	}
	return 0;
}
