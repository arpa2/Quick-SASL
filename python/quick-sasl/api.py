# Quick SASL library, with SXOVER variant support
#
# SPDX-License-Identifier: BSD-2-Clause
# Author: Rick van Rein <rick@openfortress.nl>


from importlib import import_module



class _wrapSASL (object):
	"""Abstract base class for libraries adhering to the
	   Quick SASL interface.  The subclass requests to
	   load a particular wrapper library, and this class
	   provides wrapper functions.  In addition, this
	   class can fallback to the ANONYMOUS mechanism if
	   this is permitted by an initialisation flag.
	"""

	def __init__ (self, wraplibname, service, client=True, allow_final_s2c=False):
		self._qsp = None
		self._wl = import_module (wraplibname)
		self._wl.qsasl_init (None, "Hosted_Identity") #TODO#SHARE#
		self.service = service
		self.client = client
		self.state = self._wl.new_qsaslt_state ()
		self._wl.qsaslt_state_assign (self.state, self._wl.QSA_UNDECIDED)
		self._qsp = self._wl.new_qsasl ()
		if client:
			assert not allow_final_s2c, 'The allow_final_s2c flag must be used on the server'
			assert self._wl.qsasl_client (self._qsp, service, self.state, 0), 'Failed to create SASL client'
		else:
			flags = self._wl.QSA_SERVERFLAG_ALLOW_FINAL_S2C if allow_final_s2c else 0
			assert self._wl.qsasl_server (self._qsp, service, self.state), 'Failed to create SASL server'
		self._qs = self._wl.qsasl_value (self._qsp)
		self.cbmap = {
			'tls-unique': self._wl.QSA_TLS_UNIQUE,
			'tls-server-end-point': self._wl.QSA_TLS_SERVER_END_POINT,
			'x-manual-session-key': self._wl.QSA_X_MANUAL_SESSION_KEY,
		}

	def close (self):
		"""Close the Quick SASL interface.
		"""
		#TODO#SHARED# self._wl.qsasl_fini ()
		if self._qsp is not None:
			self._wl.qsasl_close (self._qsp)
			self._qsp = None

	def __del__ (self):
		"""Cleanup the Quick SASL interface.
		"""
		self.close ()

	def get_state (self):
		"""Return the current state, which is one of the values
		   self.QSA_UNDECIDED, self.QSA_SUCCESS or self.QSA_FAILURE.
		"""
		return self._wl.qsaslt_state_value (self.state)

	def set_state (self, newstate):
		"""This empty method may be overridden by subclasses,
		   if they intend to be notified of a change.  This
		   is the analogon of the callback mechanism in C.
		"""
		pass

	def step_client (self, s2c=None):
		"""Make a setp with the client.  Return a tuple with
		   (c2s,mech), either of which may be None.
		"""
		assert self.client, 'You are confusing a server for a client'
		print ('client state:', self._wl.qsaslt_state_value (self.state))
		mech = self._wl.new_dercursor ()
		c2s  = self._wl.new_dercursor ()
		print ('step_client (_qs=%r, s2c=%r, mech=%r, c2s=%r)' % (self._qs, s2c, mech, c2s))
		print ('_step_client --> %d' % self._wl.qsasl_step_client (self._qs, s2c, mech, c2s))
		print ('client state:', self._wl.qsaslt_state_value (self.state))
		mech = self._wl.dercursor_bytes (mech) if mech is not None else None
		c2s  = self._wl.dercursor_bytes (c2s ) if c2s  is not None else None
		if mech is not None:
			mech = str (mech, 'ascii')
		return (c2s,mech)

	def step_server (self, c2s=None):
		"""Make a setp with the server.  Return the token s2c
		   or the value None.
		"""
		assert not self.client, 'You are confusing a client for a server'
		s2c  = self._wl.new_dercursor ()
		print ('step_server (_qs=%r, c2s=%r, s2c=%r)' % (self._qs, c2s, s2c))
		print ('_step_server --> %d' % self._wl.qsasl_step_server (self._qs, c2s, s2c))
		s2c   = self._wl.dercursor_bytes (s2c  ) if s2c   is not None else None
		return s2c

	def set_chanbind (self, bindtype, bindvalue, enforce=False):
		"""Set the channel binding information for a given type.
		"""
		bindtype = self.cbmap [bindtype]
		assert self._wl.qsasl_set_chanbind (self._qs, bindtype, enforce, bindvalue)

	def set_server_realm (self, srealm):
		"""Set the server realm.
		"""
		assert self._wl.qsasl_set_server_realm (self._qs, bytes (srealm, 'ascii'))

	def set_clientid_external (self, clientid):
		"""Set the client identity (for SASL EXTERNAL).
		"""
		assert self._wl.qsasl_set_clientid_external (self._qs, bytes (clientid, 'utf-8'))

	def set_client_realm (self, crealm):
		"""Set the client realm.
		"""
		assert self._wl.qsasl_set_client_realm (self._qs, bytes (crealm, 'ascii'))

	def set_clientuser_login (self, user_login):
		"""Set the client username for authentication
		   (the "login user").
		"""
		assert self._wl.qsasl_set_clientuser_login (self._qs, bytes (user_login, 'utf-8'))

	def set_clientuser_acl (self, user_acl):
		"""Set the client username for Access Control
		   (the "acl user").
		"""
		assert self._wl.qsasl_set_clientuser_acl (self._qs, bytes (user_acl, 'utf-8'))

	def get_mech (self):
		"""Get the mechanism(s) that is/are currently possible.
		"""
		mech = self._wl.new_dercursor ()
		assert self._wl.qsasl_get_mech (self._qs, mech)
		mech = self._wl.dercursor_bytes (mech)
		if mech is not None:
			mech = str (mech, 'ascii')
		return mech

	def set_mech (self, *mech):
		"""Set the mechanim(s) that are to be considered possible.
		"""
		mech = bytes (' '.join (mech), 'ascii')
		print ('set_mech (%r, %r)' % (self._qs, mech))
		assert self._wl.qsasl_set_mech (self._qs, mech)
		print ('Mechanism is now', self.get_mech ())

	# def set_clientid (self, clientid):
	# 	"""Set the client identity.
	# 	   TODO: Just for SASL EXTERNAL, or general authzid?
	# 	"""
	# 	assert self._wl.qsasl_set_clientid (self._qs, clientid)

	# def get_clientid (self):
	# 	"""Get the Client Identity, usually after success.
	# 	   The result has been subjected to SASL security.
	# 	   For ANONYMOUS, this would return None, becuase
	# 	   no client identity has been established.
	# 	"""
	# 	clientid = self._wl.new_dercursor ()
	# 	assert self._wl.qsasl_get_clientid (self._qs, clientid)
	# 	return self._wl.dercursor_bytes (clientid)

	#NAH# def get_clienthint (self):


class QuickSASL (_wrapSASL):
	"""Quick SASL is a general wrapper around SASL functions
	   adhering to the Quick SASL interface.
	"""

	def __init__ (self, service, client=True, allow_final_s2c=False):
		super (QuickSASL,self).__init__ ('_quicksasl',
		            service, client=client, allow_final_s2c=allow_final_s2c)


class XoverSASL (_wrapSASL):
	"""Xover SASL extends the SASL mechanisms available with
	   the SXOVER mechanism, used for realm crossover.  In
	   this mode, the crossover code is a wrapper around the
	   Quick SASL interface, but this wrapper exhibits the
	   same interface.
	"""

	def __init__ (self, service, client=True, allow_final_s2c=False):
		super (XoverSASL,self).__init__ ('_xoversasl',
		            service, client=client, allow_final_s2c=allow_final_s2c)


