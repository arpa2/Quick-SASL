%module quicksasl

%include "stdint.i"
%include "typemaps.i"
%include "cpointer.i"

%{

#include <arpa2/quick-sasl.h>

static void _state_change_cb (void *cbdata, QuickSASL qsasl, qsaslt_state newstate) {
        printf ("Setting QuickSASL state to %d\n", newstate);
        printf ("cbdata pointer 0x%lx\n", (long) cbdata);
        qsaslt_state *qst = (qsaslt_state *) cbdata;
        // assert (SWIG_ConvertPtr (cbdata, &qst, SWIGTYPE_p_qsaslt_state, 0) == 0);
        printf ("QuickSASL state pointer 0x%lx\n", (long) qst);
        printf ("Old QuickSASL state was %d\n", *qst);
        *qst = newstate;
        printf ("New QuickSASL state set\n");
        printf ("New QuickSASL state was %d\n", *qst);
}

PyObject *dercursor_bytes (dercursor *crs) {
        PyObject *me;
        if (crs->derptr == NULL) {
                printf ("Not storing for NULL, returning None instead\n");
                me = Py_None;
        } else {
                printf ("Storing size %zd in bytes from %lx\n", crs->derlen, (long) crs->derptr);
                me = PyBytes_FromStringAndSize ((const char *) crs->derptr, (Py_ssize_t) crs->derlen);
                printf ("Allocated me = 0x%lx\n", (long) me);
        }
        return me;
}

%}

/* Construct new_T() and T_value() functions -- as well as refcount support */
%pointer_functions (QuickSASL,qsasl);
%pointer_functions (qsaslt_state,qsaslt_state);
%pointer_functions (dercursor,dercursor);

%typemap(in)
        (void (*opt_cb) (void *cbdata, QuickSASL qsasl, qsaslt_state newstate),void *cbdata) {
/* DONE IN _wrap_qsaslt_state_value():
  if (!PyArg_ParseTuple(args,(char *)"O:qsaslt_state_value",&obj0)) SWIG_fail;
  res1 = SWIG_ConvertPtr(obj0, &argp1,SWIGTYPE_p_qsaslt_state, 0 |  0 );
  if (!SWIG_IsOK(res1)) {
    SWIG_exception_fail(SWIG_ArgError(res1), "in method '" "qsaslt_state_value" "', argument " "1"" of type '" "qsaslt_state *""'"); 
  }
  arg1 = (qsaslt_state *)(argp1);
  result = (qsaslt_state)qsaslt_state_value(arg1);
 */
/* DONE IN _wrap_qsasl_client():
  if (!PyArg_ParseTuple(args,(char *)"OOO:qsasl_client",&obj0,&obj1,&obj2)) SWIG_fail;
  res1 = SWIG_ConvertPtr(obj0, &argp1,SWIGTYPE_p_p_qsaslt_connection, 0 |  0 );
  if (!SWIG_IsOK(res1)) {
    SWIG_exception_fail(SWIG_ArgError(res1), "in method '" "qsasl_client" "', argument " "1"" of type '" "QuickSASL *""'"); 
  }
  arg1 = (QuickSASL *)(argp1);
  res2 = SWIG_AsCharPtrAndSize(obj1, &buf2, NULL, &alloc2);
  if (!SWIG_IsOK(res2)) {
    SWIG_exception_fail(SWIG_ArgError(res2), "in method '" "qsasl_client" "', argument " "2"" of type '" "char const *""'");
  }
  arg2 = (char *)(buf2);
 */
                printf ("Type mapping a state change callback\n");
                $1 = _state_change_cb;
                printf ("Input pointer is 0x%lx\n", (long) $input);
                // printf (" +--> is it NULL? %d or %d\n", !$input, $input == NULL);
                // printf (" +--> is it None? %d\n", $input == Py_None);
                // printf (" +--> its sobj is 0x%lx\n", (long) SWIG_Python_GetSwigThis($input));
                void *argp = NULL;
                //TYPE_WRONG// assert (SWIG_ConvertPtr ($input, &argp, SWIGTYPE_p_qsaslt_state, 0) != -1);
                //TRIG_ERROR// assert (SWIG_ConvertPtr ($input, &argp, 0, 0) != -1);
                SWIG_ConvertPtr ($input, &argp, 0, 0);
                printf (" +-->  argp = 0x%lx\n", (long)  argp);
                $2 = (void *) argp;
                printf ("Passd pointer is 0x%lx\n", (long) $2);
        }

%typemap(in)
        (dercursor) {
                printf ("Type mapping a dercursor\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing DER_NULL\n");
                        $1.derptr = NULL;
                        $1.derlen = 0;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS);
                        $1.derptr = (uint8_t *) view.buf;
                        $1.derlen = (size_t)    view.len;
                }
        }

%typemap(in)
        (membuf) {
                printf ("Type mapping a membuf\n");
                if ($input == Py_None) {
                        printf ("Recognised Py_None, passing NULL\n");
                        $1.bufptr = NULL;
                        $1.buflen = 0;
                } else {
                        Py_buffer view;
                        PyObject_GetBuffer ($input, &view, PyBUF_ANY_CONTIGUOUS);
                        $1.bufptr = (uint8_t *) view.buf;
                        $1.buflen = (size_t)    view.len;
                }
        }

/*
%typemap(in)
        (dercursor *) {
                $1 = malloc (sizeof (dercursor));
                assert ($1 != NULL);    //TODO:Exception//
                printf ("Initialising to-be-output dercursor 0x%lx to DER_NULL\n", $1);
                $1->derptr = NULL;
                $1->derlen = 0;
        }
%typemap(argout)
        (dercursor *) {
                printf ("Type mapping an output dercursor 0x%lx\n", $result);
                if ($result->derptr == NULL) {
                        printf ("Recognised DER_NULL, passing Py_None\n");
                        $1 = Py_None;
                } else {
                        printf ("Passing out %.*s\n", $result->derlen, $result->derptr);
                        $1 = PyBytes_FromStringAndSize ((const char *) $result->derptr, (Py_ssize_t) $result->derlen);
                        assert ($1 != NULL);    //TODO:Exception//
                }
                free ($result);
        }
*/

/*
%typemap(in)
        // NOTE: %typemap(freearg) below must mirror this one
        (derarray) {
                printf ("Type mapping a derarray\n");
                $1.dercnt = PyList_Size ($input);
                $1.derray = malloc ($1.dercnt * sizeof (union dernode));
                assert ($1.derray != NULL);     //TODO:Exception//
                int i;
                for (i=0; i<$1.dercnt; i++) {
                        PyObject *elem = PyList_GetItem ($input, i);
                        assert (elem != NULL);  //TODO:Exception//
                        Py_buffer view;
                        PyObject_GetBuffer (elem, &view, PyBUF_ANY_CONTIGUOUS);
                        $1.derray [i].wire.derptr = (uint8_t *) view.buf;
                        $1.derray [i].wire.derlen = (size_t)    view.len;
                }
        }
%typemap(freearg)
        // NOTE: %typemap(in) above allocated memory to clean up here
        (derarray) {
                printf ("free(0x%lx)\n", (long) $1.derray);
                free ($1.derray);
                printf ("free()d\n");
        }
*/


%include <arpa2/quick-sasl.h>

/* The function that replaces crashing autogenerated dercursor_bytes() */
PyObject *dercursor_bytes (dercursor *crs);

