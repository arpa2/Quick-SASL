#!/usr/bin/env python3
#
# Testing stuff...
#
# SPDX-License-Identifier: BSD-2-Clause
# Author: Rick van Rein <rick@openfortress.nl>

import api

print ('dir = %r' % (dir (api),))

qs = api.QuickSASL ('http')

# qs.set_mech ('ANONYMOUS', 'EXTERNAL', 'SCRAM-SHA-256')
# qs.set_mech ('ANONYMOUS')
# qs.set_mech ('SCRAM-SHA-256', 'DIGEST-MD5', 'ANONYMOUS')
# qs.set_mech ('DIGEST-MD5')
qs.set_mech ('CRAM-MD5')
# qs.set_mech ('PLAIN')
# qs.set_mech ('LOGIN')

#WORKS# out = qs.step_client (s2c=b'hahaha', extra=b'jajaja')
out = qs.step_client (s2c=None, extra=None)

print ('out = %r' % (out,))

print ('CLOSING, HOPEFULLY WITHOUT TOO MUCH DISMAY')

qs.close ()

print ('LEAVING, HOPEFULLY WITHOUT TOO MUCH DISMAY')

