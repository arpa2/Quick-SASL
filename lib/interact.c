/* Passphrase backend for Cyrus-SASL2
 *
 * This uses the general concept of "pinentry" designed for GnuPG, along
 * with the various implementations dedicated to a user interface context.
 *
 * Since pinentry runs as a separate program, GPL hygiene is taken care of.
 *
 * Minimal example interaction after connect:
 * S: OK Pleased to meet you
 * C: GETPIN
 * S: D 1234
 * C: OK
 * S: BYE
 * C: OK closing connection
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <unistd.h>
#include <limits.h>
#include <errno.h>

#ifndef _WIN32
#include <sys/wait.h>
#include <sys/stat.h>


#include <arpa2/quick-sasl.h>


#ifndef PASS_MAX
#define PASS_MAX 1024
#endif


/* Test if support for Quick SASL passphrase entry is
 * available.  This may check for existence of a pinentry
 * program in the local setup.
 */
bool qsasl_haveinteraction (void) {
	/* We always return true...
	if (getenv (QSASL_PASS_ENVVAR) != NULL) {
		return true;
	}
	*/
	//DEBUG// printf ("DEBUG: qsasl_haveinteraction()\n");
	//TODO// Test if the path to pinentry resolves to an executable file
	return true;
}


static char *run_pinentry (const char *cmd, size_t cmdlen, char *notok_response, char *cancel_response) {
	// Special case when envvar is set
	char *envpass = getenv (QSASL_PASS_ENVVAR);
	if (envpass != NULL) {
		if (*envpass == '\0') {
			return cancel_response;
		} else {
			return strdup (envpass);
		}
	}
	// Special case when not on a tty
	char *tty = ttyname (0);
	struct stat _st;
	if ((tty == NULL) || (*tty == '\0') || (stat ("/usr/bin/pinentry", &_st) != 0)) {
		char *buf = malloc (PASS_MAX+5);
		if (!buf) {
			return NULL;
		}
		printf ("Pipeline mode.  Pass in a single line of text.  Empty line means cancel.\npassphrase> ");
		fflush (stdout);
		if (fgets (buf, PASS_MAX+2, stdin) == NULL) {
			free (buf);
			return NULL;
		}
		int buflen = strlen (buf);
		if (buflen == 0) {
			free (buf);
			return notok_response;
		}
		if (buf [buflen-1] == '\n') {
			buflen--;
		}
		buf [buflen] = '\0';
		if (buflen == 0) {
			free (buf);
			return cancel_response;
		} else {
			return buf;
		}
	}
	// Normal case for tty operation
	int p2c [2];
	int c2p [2];
	if ((pipe (p2c) == -1) || (pipe (c2p) == -1)) {
		return NULL;
	}
	pid_t fout = fork ();
	int child_status;
	//DEBUG// printf ("fout = %d\n", fout);
	switch (fout) {
	case -1:
		/* error */
		close (p2c [0]);
		close (c2p [1]);
		perror ("Could not fork()");
		goto done;
	case 0:
		/* child */
		{ }
		close (0);
		close (1);
		dup2 (p2c [0], 0);
		dup2 (c2p [1], 1);
		close (p2c [0]);
		close (p2c [1]);
		close (c2p [0]);
		close (c2p [1]);
		//TODO// Use a CMake configuration variable with the path (default can be /usr/bin/pinentry)
		//TODO// May have to resolve link to CMake-configured variable to a real file
		//DEBUG// fprintf (stderr, "execl(pinentry)\n");
		execl ("/usr/bin/pinentry", "pinentry", "--ttyname", tty, (char *) NULL);
		perror ("Could not exec()");
		// Shouldn't get to here, bail out in disgust
		exit (1);
	default:
		/* parent */
		break;
	}
	close (p2c [0]);
	close (c2p [1]);
	/* Continue as parent */
	char *retval = NULL;
	//DEBUG// printf ("Sending \"%s\"\n", cmd);
	if (write (p2c [1], cmd, cmdlen) != cmdlen) {
		goto shutdone;
	}
	char inbuf [1024];
	ssize_t insz;
	char *pin = NULL;
	do {
		//DEBUG// printf ("Receiving\n");
		insz = read (c2p [0], inbuf, sizeof (inbuf)-1);
		if ((insz <= 0) || (insz+1 > sizeof(inbuf))) {
			perror ("Failed read()");
			goto shutdone;
		}
		inbuf [insz] = '\0';
		//DEBUG// printf ("Received %d bytes\n", insz);
		if ((inbuf [0] == 'D') && (inbuf [1] == ' ')) {
			pin = inbuf + 2;
		} else if (strncmp (inbuf, "ERR 83886179 ", 13) == 0) {
			//DEBUG// printf ("Returning CANCEL RESPONSE\n");
			retval = cancel_response;
			goto shutdone;
		} else if (strncmp (inbuf, "ERR 83886142 ", 13) == 0) {
			//DEBUG// printf ("Returning CANCEL RESPONSE due to TIMEOUT\n");
			retval = cancel_response;
			goto shutdone;
		} else if (strncmp (inbuf, "ERR 83886194 ", 13) == 0) {
			//DEBUG// printf ("Returning NOTOK RESPONSE\n");
			retval = notok_response;
			goto shutdone;
		} else {
			pin = strstr (inbuf, "\nD ");
			if (pin != NULL) {
				pin += 4;
			} else if (strstr (inbuf, "\nERR 83886179 ") != NULL) {
				//DEBUG// printf ("Returning CANCEL RESPONSE\n");
				retval = cancel_response;
				goto shutdone;
			} else if (strstr (inbuf, "\nERR 83886142 ") != NULL) {
				//DEBUG// printf ("Returning CANCEL RESPONSE due to TIMEOUT\n");
				retval = cancel_response;
				goto shutdone;
			} else if (strstr (inbuf, "\nERR 83886194 ") != NULL) {
				//DEBUG// printf ("Returning NOTOK RESPONSE\n");
				retval = notok_response;
				goto shutdone;
			} else {
				//DEBUG// printf ("Failed strstr() on \"%s\"\n", inbuf);
			}
		}
	} while (pin == NULL);
	int pinlen = 0;
	inbuf [insz] = '\n';
	while ((pin [pinlen] != '\r') && (pin [pinlen] != '\n')) {
		pinlen++;
	}
	retval = strndup (pin, pinlen);
	/* Cleanup the client, then return success or failure */
shutdone:
	waitpid (fout, &child_status, 0);
	/* Return success or failure */
done:
	close (p2c [1]);
	close (c2p [0]);
	memset (inbuf, 0, sizeof (inbuf));
	return retval;
}


/* First usage pattern: passphrase entry.  The return value
 * is NULL on failure (with errno set) or a NUL-terminated
 * string which must be cleared and free()d when done.
 *
 * SETTIMEOUT 30
 * SETDESC Passphrase for https://github.com Realm ARPA2
 * SETPROMPT [user@]pass
 * SETTITLE Login with HTTP-SASL
 * SETKEYINFO s/AABBCCDD
 * SETOK Login
 * SETCANCEL Anonymous
 * GETPIN
 *
 * The parameters should be provided by the context, in
 * desc_for, prompt, title.  The desc_for is printed after
 * "Passphrase for " and since this value is expected to
 * identify the resource to login to, it is turned to hex
 * for use in SETKEYINFO, which may be used to store the
 * key in a system-specific password storage mechanism.
 *
 * The text for Login and Cancel buttons may be replaced.
 * A useful idea is to replace Cancel with something
 * positive, like "Anonymous".  To facilitate this, the
 * Cancel operation returns a fixed string that must not
 * be free()d, holding the pointer fixptr_cancel.
 */
char *qsasl_getpassphrase (char *title, char *desc_for, const char *prompt, char *login, char *cancel, char *fixptr_cancel) {
	//DEBUG// printf ("DEBUG: qsasl_getpassphrase()\n");
	char cmd [4097];
	int cmdlen = 0;
	// Add a 30s timeout value
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETTIMEOUT 30\r\n");
	// Set the description, if present, and also KEYINFO
	if (desc_for != NULL) {
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETDESC Passphrase for %s\r\n", desc_for);
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETKEYINFO s/");
		while (*desc_for != '\0') {
			cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
				"%02x", *desc_for);
			desc_for++;
		}
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"\r\n");
	}
	// Set the title, if present
	if (title != NULL) {
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETTITLE %s\r\n", title);
	}
	// Set the prompt, if present
	if (prompt != NULL) {
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETPROMPT %s\r\n", prompt);
	}
	// Set the OK button to "Login"
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETOK %s\r\n", (login != NULL) ? login : "Login");
	// Set the Cancel button to "Anonymous" if that fallback is offered
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETCANCEL %s\r\n", (cancel != NULL) ? cancel : "Cancel");
	// Ask for the PIN
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"GETPIN\r\n");
	// Say goodbye
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"BYE\r\n");
	// Now perform the pinentry interaction based on this command
	return run_pinentry (cmd, cmdlen, NULL, fixptr_cancel);
}


/* Second usage pattern: Ask for Confirmation.
 *
 * SETTIMEOUT 30
 * SETDESC Login as vanrein@github.com
 * SETTITLE Approval for Client Identity
 * SETOK OK
 * SETNOTOK Change
 * SETCANCEL Cancel
 * CONFIRM
 *
 * The desc and title values should be supplied.  Alternate
 * texts for OK and Cancel buttons may be given.  The return
 * value is:
 *  1 for OK
 *  0 for NOTOK
 * -1 for CANCEL or TIMEOUT
 */
int qsasl_confirm (char *title, char *desc, char *ok, char *notok, char *cancel) {
	//DEBUG// printf ("DEBUG: qsasl_confirm()\n");
	char cmd [4097];
	int cmdlen = 0;
	// Add a 30s timeout value
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETTIMEOUT 30\r\n");
	// Set the title, which ought to be provided
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETTITLE %s\r\n", title);
	// Set the description, which ought to be given
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETDESC %s\r\n", desc);
	// Set the OK button text, if it is given
	if (ok != NULL) {
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETOK %s\r\n", ok);
	}
	// Set the NOTOK button text, if it is given
	if (notok != NULL) {
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETNOTOK %s\r\n", notok);
	}
	// Set the Cancel button text, if it is given
	if (cancel != NULL) {
		cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"SETCANCEL %s\r\n", cancel);
	}
	// Ask for confirmation
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"CONFIRM\r\n");
	// Say goodbye
	cmdlen += snprintf (cmd+cmdlen, sizeof(cmd)-cmdlen-1,
			"BYE\r\n");
	// Now perform the pinentry interaction based on this command
	char *fixptr_cancel = "CANCEL";
	char *fixptr_notok  = "NOT_OK";
	char *pin = run_pinentry (cmd, cmdlen, fixptr_notok, fixptr_cancel);
	if (pin == fixptr_cancel) {
		return -1;
	} else if (pin == fixptr_notok) {
		return 0;
	} else {
		return 1;
	}
}


#if 0
int main (int argc, char *argv []) {
	printf ("TTY is %s\n", ttyname (0));
	printf ("qsasl_haveinteraction() --> %d\n", qsasl_haveinteraction ());
	char *anon_resp = "ANON";
	char *pin = qsasl_getpassphrase ("Login with HTTP-SASL", "https://github.com Realm ARPA2", "[user@]pass", "Anonymous", NULL, anon_resp);
	printf ("qsasl_getpassphrase () --> \"%s\"\n", pin);
	if (pin == anon_resp) {
		printf ("Squashing ANONYMOUS\n");
		pin = NULL;
	}
	free (pin);
	switch (qsasl_confirm ("Would you agree?", "Describing what we mean", "Aye", "Nay", "Bail out")) {
	case 0:
		printf ("Nah, better not\n");
		break;
	case 1:
		printf ("Yeah, go for it\n");
		break;
	case -1:
		printf ("EEEEEEEEKKKKKKK\n");
		break;
	}
}
#endif

#else /* _WIN32 */
bool qsasl_haveinteraction (void) {
	/* We always return true...
	if (getenv (QSASL_PASS_ENVVAR) != NULL) {
		return true;
	}
	*/
	//DEBUG// printf ("DEBUG: qsasl_haveinteraction()\n");
	//TODO// Test if the path to pinentry resolves to an executable file
	return true;
}
// Windows stubs
char *qsasl_getpassphrase (char *title, char *desc_for, char *prompt, char *login, char *cancel, char *fixptr_cancel) {
	char *qsasl_sleep  = getenv("QUICKSASL_SLEEP");
	if (qsasl_sleep) {
		sleep(atoi(qsasl_sleep));
	}
	char *passphrase = getenv("QUICKSASL_PASSPHRASE");
	return strdup(passphrase ? passphrase : "sekreet");
}

int qsasl_confirm (char *title, char *desc, char *ok, char *notok, char *cancel) {
	return 1;
}
#endif /* _WIN32 */
