/* Cyrus-SASL2 drivers for Quick SASL.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <errno.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>

#include <arpa2/identity.h>
#include <arpa2/access_actor.h>
#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>
#include <arpa2/except.h>

#include "sasl-dbg.h"

struct qsaslt_connection {
	sasl_conn_t *handle;
	const char *service;
	char *srealm;
	char *crealm;
	char *user_login;
	char *user_acl;
	char *postmech_user;
	char *mechlist;
	void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate);
	void *cbdata;
	bool is_server;
	bool allow_final_s2c;
	bool started;
	bool succeeded;
	bool failed;
	//
	// Channel binding type code and value
	const char *chanbindnam;
	membuf chanbindval;
};


/* improved callback to verify authorization;
 *     canonicalization now handled elsewhere
 *  conn           -- connection context
 *  requested_user -- the identity/username to authorize (NUL terminated)
 *  rlen           -- length of requested_user
 *  auth_identity  -- the identity associated with the secret (NUL terminated)
 *  alen           -- length of auth_identity
 *  default_realm  -- default user realm, as passed to sasl_server_new if
 *  urlen          -- length of default realm
 *  propctx        -- auxiliary properties
 * returns SASL_OK on success,
 *         SASL_NOAUTHZ or other SASL response on failure
 */
int arpa2_acluser (sasl_conn_t *conn, void *context,
			const char *requested_user, unsigned rlen,
			const char *auth_identity, unsigned alen,
			const char *def_realm, unsigned urlen,
			struct propctx *propctx) {
	log_debug ("Matching user_acl=%.*s#%d to user_login=%.*s#%d in realm %.*s",
			rlen, requested_user, rlen, alen, auth_identity, alen, urlen, def_realm);
	//
	// Try to parse both identifiers as ARPA2 Identity
	// Question: Is it an improvement to attach realm to auth_identity?
	// Question: Is it an improvement to allow local-part in requested_user?
	// Answers:  For now, we'll demand users to provide user@domain.name!
	a2id_t current_id;
	a2id_t desired_id;
	bool bothok = true;
	bothok = bothok && a2id_parse (&current_id, auth_identity,  alen);
	bothok = bothok && a2id_parse (&desired_id, requested_user, rlen);
	//
	// Simplicity fallback: Names must be equal, modulo case.
	//                      No fancy treatment without ARPA2.
	if (!bothok) {
		if (strcasecmp (requested_user, auth_identity) == 0) {
			return SASL_OK;
		} else {
			return SASL_NOAUTHZ;
		}
	}
	//
	// Given a realm, we should use ARPA2 Identity and ARPA2 Access Control
	// and more specifically, the access_actor() function to test for
	// the ability to switch to a desired Actor Identity
	if (access_actor (&current_id, &desired_id)) {
		return SASL_OK;
	} else {
		return SASL_NOAUTHZ;
	}
}


/* logging callback -- this allows plugins and the middleware to
 *  log operations they perform.
 * inputs:
 *  context     -- logging context from the callback record
 *  level       -- logging level; see above
 *  message     -- message to log
 * returns:
 *  SASL_OK     -- no error
 *  SASL_FAIL   -- error
 */
int arpa2_logging (void *context, int level, const char *message) {
	switch (level) {
	case SASL_LOG_ERR:
	case SASL_LOG_FAIL:
		log_error ("%s", message);
		break;
	case SASL_LOG_WARN:
		log_warning ("%s", message);
		break;
	case SASL_LOG_NOTE:
		log_notice ("%s", message);
		break;
	case SASL_LOG_DEBUG:
	case SASL_LOG_TRACE:
		log_debug ("%s", message);
		break;
	case SASL_LOG_NONE:
	case SASL_LOG_PASS:
	default:
		break;
	}
	return 0;
}


/* callback for a server-supplied user canonicalization function.
 *
 * This function is called directly after the mechanism has the
 * authentication and authorization IDs.  It is called before any
 * User Canonicalization plugin is called.  It has the responsibility
 * of copying its output into the provided output buffers.
 *
 *  in, inlen     -- user name to canonicalize, may not be NUL terminated
 *                   may be same buffer as out
 *  flags         -- not currently used, supplied by auth mechanism
 *  user_realm    -- the user realm (may be NULL in case of client)
 *  out           -- buffer to copy user name
 *  out_max       -- max length of user name
 *  out_len       -- set to length of user name
 *
 * returns
 *  SASL_OK         on success
 *  SASL_BADPROT    username contains invalid character
 */
int arpa2_canon_user (sasl_conn_t *conn,
                              void *context,
                              const char *in, unsigned inlen,
                              unsigned flags,
                              const char *user_realm,
                              char *out,
                              unsigned out_max, unsigned *out_len) {
	char fulluser [inlen + 1 + strlen (user_realm) + 1];
	memcpy (fulluser, in, inlen);
	fulluser [inlen] = '\0';
	if (memchr (fulluser, '@', inlen) == NULL) {
		fulluser [inlen] = '@';
		strcpy (fulluser + inlen + 1, user_realm);
	}
	a2id_t canonuser;
	if (!a2id_parse (&canonuser, fulluser, 0)) {
		return SASL_BADPROT;
	}
	int canonlen = canonuser.ofs [A2ID_OFS_END];
	if (canonlen > out_max) {
		return SASL_NOMEM;
	}
	memcpy (out, canonuser.txt, canonlen);
	*out_len = canonlen;
	return SASL_OK;
}


#ifdef _WIN32
static int getpath(void *context __attribute__((unused)), char ** path)
{
	if (! path)
		return SASL_BADPARAM;

	char *sasl_path = getenv("SASL_PATH");
	if (sasl_path) {
		*path = sasl_path;
	}
	log_debug ("SASL_PATH=%s", sasl_path);
	return SASL_OK;
}

static int getconfpath(void *context __attribute__((unused)), char ** path)
{
	if (! path)
		return SASL_BADPARAM;

	char *sasl_conf_path = getenv("SASL_CONF_PATH");
	if (sasl_conf_path) {
		*path = sasl_conf_path;
	}
	log_debug ("SASL_CONF_PATH=%s", sasl_conf_path);
	return SASL_OK;
}
#endif /* _WIN32 */

/* The callbacks that can be supported (via SASL_INTERACT) */
static sasl_callback_t qsasl_client_callbacks [] = {
	{ SASL_CB_LOG, (int(*)(void)) arpa2_logging, NULL },
#ifdef _WIN32
	{ SASL_CB_GETPATH, (sasl_callback_ft)&getpath, NULL },
	{ SASL_CB_GETCONFPATH, (sasl_callback_ft)&getconfpath, NULL },
#endif /* _WIN32 */
	{ SASL_CB_GETREALM, NULL, NULL },
	{ SASL_CB_USER, NULL, NULL },
	{ SASL_CB_AUTHNAME, NULL, NULL },
	{ SASL_CB_PASS, NULL, NULL },
	{ SASL_CB_CANON_USER, (int(*)(void)) arpa2_canon_user, NULL },
	{ SASL_CB_LIST_END, NULL, NULL }
 };
//
static sasl_callback_t qsasl_server_callbacks [] = {
	{ SASL_CB_LOG, (int(*)(void)) arpa2_logging, NULL },
#ifdef _WIN32
	{ SASL_CB_GETPATH, (sasl_callback_ft)&getpath, NULL },
	{ SASL_CB_GETCONFPATH, (sasl_callback_ft)&getconfpath, NULL },
#endif /* _WIN32 */
	{ SASL_CB_PROXY_POLICY, (int(*)(void)) arpa2_acluser, NULL },
	{ SASL_CB_CANON_USER, (int(*)(void)) arpa2_canon_user, NULL },
	{ SASL_CB_LIST_END, NULL, NULL }
};


/* Global Initialisation of Quick SASL.
 *
 * If you need to provide a driver library hint,
 * this is where to do it.  Not actually used yet.
 *
 * The default appname is "Hosted_Identity".
 */
bool qsasl_init (const char *_opt_driver, const char *appname) {
	if (appname == NULL) {
		appname = "Hosted_Identity";
	}
	sasl_callback_t *cli_cb = qsasl_haveinteraction () ? qsasl_client_callbacks : NULL;
	log_debug ("sasl_client_init (%p)", cli_cb);
        int cyres = sasl_client_init (cli_cb);
	if (cyres == SASL_OK) {
		cyres = sasl_server_init (qsasl_server_callbacks, appname);
		if (cyres != SASL_OK) {
			sasl_client_done ();
		}
	}
        return (cyres == SASL_OK);
}


/* Global Cleanup of Quick SASL.
 */
void qsasl_fini (void) {
        sasl_done ();
}


/* Create a Client Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_CLIENTFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 */
bool qsasl_client (QuickSASL *out_qsasl,
		const char *service,
                void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate),
		void *cbdata,
		unsigned _flags) {
	if (!mem_open (sizeof (struct qsaslt_connection), (void **) out_qsasl)) {
		/* errno is set to ENOMEM */
		return false;
	}
	(*out_qsasl)->opt_cb  = opt_cb;
	(*out_qsasl)->cbdata  = cbdata;
	(*out_qsasl)->service = service;
	log_debug ("qsasl_client at %p..%p", *out_qsasl, ((uint8_t *) *out_qsasl) + sizeof (struct qsaslt_connection) -1);
	// Note: No meaningful flags at this time
	return true;
}


/* Create a Server Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_SERVERFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 *
 * This routine differs from qsasl_client() only in
 * that it sets the distinguishing flag is_server.
 *
 * Prior to this call, *out_qsasl must be NULL.
 */
bool qsasl_server (QuickSASL *out_qsasl,
		const char *service,
                void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate),
		void *cbdata,
		unsigned flags) {
	if (!qsasl_client (out_qsasl, service, opt_cb, cbdata, 0)) {
		return false;
	}
	(*out_qsasl)->is_server = true;
	if (flags & QSA_SERVERFLAG_ALLOW_FINAL_S2C) {
		(*out_qsasl)->allow_final_s2c = true;
	}
	return true;
}


/* Close the Client/Server Connection Object.
 *
 * This frees the SASL connection handle, plus
 * any data that was allocated in its pool.
 *
 * After this call, *inout_qsasl will be NULL.
 */
void qsasl_close (QuickSASL *inout_qsasl) {
	if ((*inout_qsasl)->handle != NULL) {
		sasl_dispose (&(*inout_qsasl)->handle);
	}
	mem_close ((void **) inout_qsasl);
}


/* Internal.  Open the connection if not done yet.
 */
static bool _qsasl_have (QuickSASL qsasl) {
	int cyres = SASL_FAIL;
	if (qsasl->handle != NULL) {
		cyres = SASL_OK;
	} else if (qsasl->is_server) {
		log_info ("Starting server @%s for clients @%s", qsasl->srealm, qsasl->crealm);
		assert (qsasl->srealm != NULL);
		assert (qsasl->crealm != NULL);
		unsigned flags = 0;
		if (qsasl->allow_final_s2c) {
			flags |= SASL_SUCCESS_DATA;
		}
		cyres = sasl_server_new (qsasl->service,
				qsasl->srealm, qsasl->crealm, NULL, NULL,
				/*cnx-spec-cb: */ NULL, flags,
				&qsasl->handle);
	} else {
		log_info ("Starting client for service %s@%s", qsasl->service, qsasl->srealm);
		assert (qsasl->srealm != NULL);
		cyres = sasl_client_new (qsasl->service,
				qsasl->srealm, NULL, NULL,
				/*cnx-spec-cb: */ NULL, 0,
				&qsasl->handle);
	}
	log_debug ("_qsasl_have() returns %s (%d)", sasl_errstring (cyres, NULL, NULL), cyres);
	return (cyres == SASL_OK);
}

/* Set the Channel Binding information for a given type.
 */
static const char *chanbindnames [] = QSASL_CHANBIND_STRINGS;

static bool find_prefix(membuf value, const char *prefix)
{
	int prefix_len = strlen(prefix);
	return value.buflen > prefix_len && memcmp(value.bufptr, prefix, prefix_len) == 0 && value.bufptr[prefix_len] == ':';
}

static int find_prefix_index(membuf value)
{
	int index = 0;
	const char **prefix = chanbindnames;
	while (*prefix) {
		if (find_prefix(value, *prefix++)) {
			return index;
		}
		index++;
	}
	return -1;
}

/* Set the Channel Binding directly
 * value contains a channel binding with a prefix
 * https://datatracker.ietf.org/doc/html/rfc5056#section-2.1
 */
bool qsasl_set_chanbind (QuickSASL qsasl, bool enforce, membuf value) {
	assert (value.bufptr != NULL);
	const char *bind_name = NULL;
	if (!_qsasl_have (qsasl)) {
		log_errno ("!_qsasl_have()");
		return false;
	}
	struct sasl_channel_binding *chanbind = NULL;
	if (!qsasl_alloc (qsasl, sizeof (*chanbind), (uint8_t **) &chanbind)) {
		/* errno is set to ENOMEM */
		log_errno ("!qsasl_alloc()");
		return false;
	}
	int index = find_prefix_index(value);
	if (index < 0) {
		return false;
	}
	bind_name = chanbindnames [index];
	chanbind->name     = bind_name;
	chanbind->critical = enforce;
	chanbind->len      = value.buflen;
	chanbind->data     = value.bufptr;
	log_data("chanbind: ", chanbind->data, chanbind->len, 0);
	if (sasl_setprop (qsasl->handle, SASL_CHANNEL_BINDING, chanbind) != SASL_OK) {
		errno = ENOSYS;	//TODO//
		log_errno ("sasl_setprop()");
		return false;
	}
	qsasl->chanbindnam = bind_name;
	qsasl->chanbindval = value;
	return true;
}


/* Get the Channel Binding value.
 */
bool qsasl_get_chanbind_value (QuickSASL qsasl, membuf *value) {
	*value = qsasl->chanbindval;
	return (qsasl->chanbindnam != NULL);
}


/* Get the Channel Binding name.
 */
bool qsasl_get_chanbind_name (QuickSASL qsasl, const char **name) {
	*name = qsasl->chanbindnam;
	return (*name != NULL);
}


/* Set the Client Identity (for SASL EXTERNAL).
 */
bool qsasl_set_clientid_external (QuickSASL qsasl, membuf clientid) {
	assert (clientid.bufptr != NULL);
	char *clibuf = NULL;
	if (!mem_strdup (qsasl, clientid, &clibuf)) {
		/* errno is set to ENOMEM */
		return false;
	}
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	if (sasl_setprop (qsasl->handle, SASL_AUTH_EXTERNAL, clibuf) != SASL_OK) {
		errno = ENOSYS; //TODO//
		return false;
	}
	return true;
}


/* Set the Client username for authentication (the "login user").
 */
bool qsasl_set_clientuser_login (QuickSASL qsasl, membuf user_login) {
	assert (user_login.bufptr != NULL);
	assert (qsasl->user_login == NULL);
	log_debug ("Setting QuickSASL user_login=%.*s in %p", (int)user_login.buflen, user_login.bufptr, qsasl);
	/* errno will be set to ENOMEM on failure */
	return mem_strdup (qsasl, user_login, &qsasl->user_login);
}


/* Set the Client username to act for (the "ACL user").
 */
bool qsasl_set_clientuser_acl (QuickSASL qsasl, membuf user_acl) {
	assert (user_acl.bufptr != NULL);
	assert (qsasl->user_acl == NULL);
	log_debug ("Setting QuickSASL user_acl=%.*s in %p", (int)user_acl.buflen, user_acl.bufptr, qsasl);
	/* errno will be set to ENOMEM on failure */
	return mem_strdup (qsasl, user_acl, &qsasl->user_acl);
}


/* Following is client userid/domain fetching code.  This is made
 * to unravel the user@realm code above, which follows what is
 * stated in <sasl/sasl.h>:
 *
 *************************************************
 * IMPORTANT NOTE: server realms / username syntax
 *
 * If a user name contains a "@", then the rightmost "@" in the user name
 * separates the account name from the realm in which this account is
 * located.  A single server may support multiple realms.  If the
 * server knows the realm at connection creation time (e.g., a server
 * with multiple IP addresses tightly binds one address to a specific
 * realm) then that realm must be passed in the user_realm field of
 * the sasl_server_new call.  If user_realm is non-empty and an
 * unqualified user name is supplied, then the canon_user facility is
 * expected to append "@" and user_realm to the user name.  The canon_user
 * facility may treat other characters such as "%" as equivalent to "@".
 *
 * If the server forbids the use of "@" in user names for other
 * purposes, this simplifies security validation.
 */


/* Have the post-mechanism client identity in user[@realm] form.
 * Return true when the string should be processed, rather than a
 * failure of the call.  The string is explicitly provided as
 * absent for the exceptional case of ANONYMOUS "authentication".
 */
static bool _have_postmech_user (QuickSASL qsasl) {
	if (qsasl->postmech_user != NULL) {
		return true;
	}
	if (qsasl->mechlist == NULL) {
		/* No mechanism determined yet */
		log_debug ("No post-mechanism user due to missing mechanism list");
		return false;
	}
	if (strchr (qsasl->mechlist, ' ') != NULL) {
		/* Mechanism list not focussed on one yet */
		log_debug ("No post-mechanism user due to missing mechanism choice");
		return false;
	}
	if (strcmp (qsasl->mechlist, "ANONYMOUS") == 0) {
		/* One mechanism, but no authenticated client identity */
		return true;
	}
	int ret = sasl_getprop (qsasl->handle, SASL_USERNAME,
				(const void **) &qsasl->postmech_user);
	if (ret != SASL_OK) {
		qsasl->postmech_user = NULL;
		log_debug ("No post-mechanism user due to Cyrus error %d", ret);
		return false;
	}
	return true;
}


/* Get the Client UserID, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return bufptr==NULL,
 * because no client identity has been established.
 */
bool qsasl_get_client_userid (QuickSASL qsasl, membuf *out_userid) {
	memset (out_userid, 0, sizeof (membuf));
	if (!_have_postmech_user (qsasl)) {
		return false;
	}
	out_userid->bufptr = (uint8_t*)qsasl->postmech_user;
	out_userid->buflen = (qsasl->postmech_user) ? strlen (qsasl->postmech_user) : 0;
	if (qsasl->postmech_user == NULL) {
		/* ANONYMOUS successfully reports NULL */
		log_debug ("ANONYMOUS authentication reports NULL as userid");
		return true;
	}
	int at = out_userid->buflen;
	while (at-- > 0) {
		if (out_userid->bufptr [at] == '@') {
			out_userid->buflen = at;
			break;
		}
	}
	log_debug ("qsasl_get_client_userid() -> \"%.*s[%s]\"", at, qsasl->postmech_user, &qsasl->postmech_user [at]);
	return true;
}


/* Get the Client Domain, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this may return the realm of the
 * server if one was defined, even though no UserID
 * has been proven.
 */
bool qsasl_get_client_domain (QuickSASL qsasl, membuf *out_domain) {
	memset (out_domain, 0, sizeof (membuf));
	if (!_have_postmech_user (qsasl)) {
		return false;
	}
	if (qsasl->postmech_user == NULL) {
		/* ANONYMOUS reports the server domain */
		if (qsasl->srealm != NULL) {
			log_debug ("ANONYMOUS authentication reports the domain %s", qsasl->srealm);
			out_domain->bufptr = (uint8_t*)qsasl->srealm ;
			out_domain->buflen = strlen (qsasl->srealm);
		}
		return true;
	}
	int at = strlen (qsasl->postmech_user);
	while (at-- > 0) {
		if (qsasl->postmech_user [at] == '@') {
			break;
		}
	}
	char* domain_part = &qsasl->postmech_user [at+1];
	out_domain->bufptr = (uint8_t*)domain_part;
	out_domain->buflen = strlen (domain_part);
	log_debug ("qsasl_get_client_domain() -> \"[%.*s]%s\"", at+1, qsasl->postmech_user, out_domain->bufptr);
	return true;
}


/* Some mechanisms may delegate a credential to the
 * client.  This can be retrieved with this call.
 * The call sets DER NULL if no credential was
 * presented, but still returns true.
 */
bool qsasl_get_delegated_credential (QuickSASL qsasl, membuf *out_cred) {
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	if (sasl_getprop (qsasl->handle,
	                  SASL_DELEGATEDCREDS,
	                  (const void **) &out_cred->bufptr) != SASL_OK) {
		out_cred->bufptr = NULL;
		out_cred->buflen = 0;
		return false;
	}
	if (out_cred->bufptr == NULL) {
		out_cred->buflen = 0;
	} else {
		out_cred->buflen = strlen ((char *)out_cred->bufptr);
	}
	return true;
}


/* Set the Server Realm.
 */
bool qsasl_set_server_realm (QuickSASL qsasl, membuf srealm) {
	assert (srealm.bufptr != NULL);
	assert (qsasl->srealm == NULL);
	log_debug ("Setting QuickSASL srealm=%.*s in %p", (int)srealm.buflen, srealm.bufptr, qsasl);
	return mem_strdup (qsasl, srealm, &qsasl->srealm);
}


/* Set the Client Realm.
 */
bool qsasl_set_client_realm (QuickSASL qsasl, membuf crealm) {
	assert (crealm.bufptr != NULL);
	assert (qsasl->crealm == NULL);
	log_debug ("Setting QuickSASL crealm=%.*s in %p", (int)crealm.buflen, crealm.bufptr, qsasl);
	return mem_strdup (qsasl, crealm, &qsasl->crealm);
}


/* Get the Mechanism(s) that is/are currently possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool qsasl_get_mech (QuickSASL qsasl, membuf *out_mech) {
	assert (qsasl->is_server);
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	if (qsasl->mechlist == NULL) {
		if (sasl_listmech (qsasl->handle, NULL,
				"", " ", "",
				(const char **) &qsasl->mechlist,
				NULL, NULL) != SASL_OK) {
			return false;
		}
		log_info ("Cyrus SASL mech list %s", qsasl->mechlist);
	}
	assert (qsasl->mechlist != NULL);
	log_debug ("qsasl_get_mech () --> %s", qsasl->mechlist);
	out_mech->bufptr = (uint8_t*)qsasl->mechlist ;
	out_mech->buflen = strlen (qsasl->mechlist);
	return true;
}


/* Fallback mechanism list, used when a client or server
 * is started without a mechanism having been set.
 */
const char *mechlist_fallback_anonymous = "ANONYMOUS";


/* Set the Mechanism(s) to be considered possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool qsasl_set_mech (QuickSASL qsasl, membuf mech) {
	qsasl->mechlist = NULL;
	return mem_strdup (qsasl, mech, &qsasl->mechlist);
}

/* Determine the flags for success or failure.  They may
 * both be reset, in which case the work is not done.
 * The return value indicates success of the call that
 * fills the flags.
 */
bool qsasl_get_outcome (QuickSASL qsasl, bool *success, bool *failure) {
	assert (success != NULL);
	assert (failure != NULL);
	if (qsasl->succeeded && qsasl->failed) {
		return false;
	}
	*success = qsasl->succeeded;
	*failure = qsasl->failed;
	return true;
}

/* Make a step as a SASL Client.
 */
bool qsasl_step_client (QuickSASL qsasl, membuf s2c, membuf *out_mech, membuf *out_c2s) {
	memset (out_mech, 0, sizeof (membuf));
	memset (out_c2s,  0, sizeof (membuf));
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	const char *cliout    = NULL;
	unsigned    clioutlen = 0;
	int cyres = SASL_NOMECH;
	sasl_interact_t *prompts = NULL;
redo_after_interact:
	if (!qsasl->started) {
		const char *mechchoice = NULL;
		log_debug ("sasl_client_start() called");
		log_debug ("Trying mechlist %s", qsasl->mechlist);
		cyres = sasl_client_start (qsasl->handle,
				(qsasl->mechlist!=NULL) ? qsasl->mechlist : mechlist_fallback_anonymous,
				&prompts,
				&cliout, &clioutlen,
				&mechchoice);
		log_debug ("sasl_client_start() returns %s (%d) and mechchoice %s", sasl_errstring (cyres, NULL, NULL), cyres, mechchoice);
		//ALSO_INTERACT// if (((cyres == SASL_OK) || (cyres == SASL_CONTINUE)) && (mechchoice != NULL)) {
		if (mechchoice != NULL) {
			out_mech->bufptr = (uint8_t *) mechchoice ;
			out_mech->buflen = strlen     (mechchoice);
			qsasl->mechlist = NULL;
			if (!mem_strdup (qsasl, *out_mech, &qsasl->mechlist)) {
				cyres = SASL_NOMECH;
			}
		}
	} else {
		log_debug ("sasl_client_step() called");
		cyres = sasl_client_step (qsasl->handle,
				(char *) s2c.bufptr, s2c.buflen,
				&prompts,
				&cliout, &clioutlen);
		log_debug ("sasl_client_step() returns %s (%d)", sasl_errstring (cyres, NULL, NULL), cyres);
		if ((cyres == SASL_OK) || (cyres == SASL_CONTINUE)) {
			out_mech->bufptr = NULL;
			out_mech->buflen = 0;
		}
	}
	if (cyres == SASL_INTERACT) {
		sasl_interact_t *prompt = prompts;
		log_debug ("client is prompted for interaction with QuickSASL %p", qsasl);
		char *desc_for = (char *) qsasl->service; /*TODO*/
		while (prompt->id != SASL_CB_LIST_END) {
			log_debug ("prompt->id == 0x%lx", prompt->id);
			prompt->result = NULL;
			if (prompt->id == SASL_CB_GETREALM) {
				if (qsasl->crealm != NULL) {
					prompt->result = qsasl->crealm;
				} else {
					prompt->result = qsasl->srealm;
				}
			} else if (prompt->id == SASL_CB_USER) {
				log_info ("Replying user_acl is %s", qsasl->user_acl);
				prompt->result = qsasl->user_acl;
			} else if (prompt->id == SASL_CB_AUTHNAME) {
				log_info ("Replying user_login is %s", qsasl->user_login);
				prompt->result = qsasl->user_login;
			} else if (prompt->id == SASL_CB_PASS) {
				char *pass = qsasl_getpassphrase (
						"Quick SASL needs a Passphrase",
						qsasl->crealm,
						(char *) prompt->prompt, //"Passphrase:",
						"Login", "Cancel", NULL);
				if (pass != NULL) {
					membuf passcrs = { .bufptr=(uint8_t *)pass, .buflen=strlen (pass) };
					mem_strdup (qsasl, passcrs, (char **) &prompt->result);
					memset (pass, 0, strlen (pass));	/* do not leak */
					free (pass);
				}
			} else if ((prompt->id == SASL_CB_ECHOPROMPT) || (prompt->id == SASL_CB_NOECHOPROMPT)) {
				char *resp = qsasl_getpassphrase (
						"Quick SASL needs a Response to a Challenge",
						(char *) prompt->challenge,
						(char *) prompt->prompt, //"Response:",
						"Login", "Cancel", NULL);
				if (resp != NULL) {
					membuf respcrs = { .bufptr=(uint8_t *)resp, .buflen=strlen (resp) };
					mem_strdup (qsasl, respcrs, (char **) &prompt->result);
					memset (resp, 0, strlen (resp));	/* do not leak */
					free (resp);
				}
			} else {
				log_error ("qsasl_step_client() got unknown prompt id %ld", prompt->id);
			}
			if (prompt->result == NULL) {
				prompt->result = prompt->defresult;
				log_debug ("Callback 0x%lx prompt set to the default at address 0x%0lx", prompt->id, (long) prompt->defresult);
			} else {
				prompt->len = strlen (prompt->result);
				log_debug ("Callback 0x%lx prompt->result = (suppressed)#%d", prompt->id, prompt->len);
			}
			prompt++;
		}
		goto redo_after_interact;
	}
	if (cyres == SASL_OK) {
		log_debug ("qsasl_step_client() finishes successfully");
		assert (!qsasl->succeeded);
		assert (!qsasl->failed);
		qsasl->succeeded = true;
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, QSA_SUCCESS);
			qsasl->opt_cb = NULL;
		}
	} else if (cyres != SASL_CONTINUE) {
		log_debug ("qsasl_step_client() fails");
		assert (!qsasl->succeeded);
		assert (!qsasl->failed);
		qsasl->failed = true;
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, QSA_FAILURE);
			qsasl->opt_cb = NULL;
		}
		return true;
	}
	qsasl->started = true;
	out_c2s->bufptr = (uint8_t *) cliout   ;
	out_c2s->buflen = (size_t   ) clioutlen;
	log_debug ("qsasl_step_client() returns %s SASL token of %d bytes", cliout ? "a" : "no", clioutlen);
	return true;
}


/* Make a step as a SASL Server.
 */
bool qsasl_step_server (QuickSASL qsasl, membuf c2s, membuf *out_s2c) {
	memset (out_s2c, 0, sizeof (membuf));
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	const char *srvout    = NULL;
	unsigned    srvoutlen = 0;
	int cyres = SASL_NOMECH;
	if (!qsasl->started) {
		log_debug ("sasl_server_start() with mechlist=\"%s\" and c2s=\"%.*s\"", qsasl->mechlist, (int)c2s.buflen, c2s.bufptr);
		cyres = sasl_server_start (qsasl->handle,
				(qsasl->mechlist!=NULL) ? qsasl->mechlist : mechlist_fallback_anonymous,
				(char *) c2s.bufptr, c2s.buflen,
				&srvout, &srvoutlen);
		log_debug ("sasl_server_start() returns %s (%d)", sasl_errstring (cyres, NULL, NULL), cyres);
	} else {
		cyres = sasl_server_step (qsasl->handle,
				(char *)c2s.bufptr, c2s.buflen,
				&srvout, &srvoutlen);
		log_debug ("sasl_server_step() returns %s (%d)", sasl_errstring (cyres, NULL, NULL), cyres);
	}
	if (cyres == SASL_OK) {
		log_debug ("qsasl_step_server() finishes successfully");
		assert (!qsasl->succeeded);
		assert (!qsasl->failed);
		qsasl->succeeded = true;
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, QSA_SUCCESS);
			qsasl->opt_cb = NULL;
		}
	} else if (cyres != SASL_CONTINUE) {
		log_debug ("qsasl_step_server() fails");
		assert (!qsasl->succeeded);
		assert (!qsasl->failed);
		qsasl->failed = true;
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, QSA_FAILURE);
			qsasl->opt_cb = NULL;
		}
		return true;
	}
	qsasl->started = true;
	out_s2c->bufptr = (uint8_t *) srvout   ;
	out_s2c->buflen = (size_t   ) srvoutlen;
	log_debug ("qsasl_step_server() returns %s SASL token of %d bytes", srvout ? "a" : "no", srvoutlen);
	return true;
}
