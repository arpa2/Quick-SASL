/* DiaSASL drivers for Quick SASL.
 *
 * This wraps around DiaSASL to facilitate a server with a normal
 * QuickSASL server-side API.  Client-specific calls return failure
 * with errno set to ENOSYS.  Thanks to this wrapper, binaries can
 * be separately linked for local and nearby SASL handling.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>

#include <arpa2/except.h>
#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-mem.h>
#include <arpa2/quick-sasl.h>
#include <arpa2/quick-diasasl.h>

#include "sasl-dbg.h"


struct qsaslt_connection {
	struct diasasl_session diases;
	char *srealm;
	char *mech_list;
	char *mech_choice;
	membuf *s2c_token;
	void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate);
	void *cbdata;
	bool opened;
	bool started;
	bool succeeded;
	bool failed;
	bool allow_final_s2c;
};



/* The globally shared diasasl_node structure.
 */
static struct diasasl_node dianode;


/* Global Initialisation of Quick SASL.
 *
 * If you need to provide a driver library hint,
 * this is where to do it.  Not actually used yet.
 *
 * The default appname is "Hosted_Identity".
 */
bool qsasl_init (const char *_opt_driver, const char *appname) {
	if (appname == NULL) {
		appname = "Hosted_Identity";
	}
	//
	// Start the DiaSASL node
	diasasl_node_open (&dianode);
	dianode.socket = -1;
	//
	// Allocate the socket
	//TODO// Look into a configuration file instead of envvars?
	struct sockaddr_storage ss;
	char *host = getenv ("DIASASL_HOST");
	char *port = getenv ("DIASASL_PORT");
	if (host == NULL) {
		host = "::1";
	}
	if (!socket_parse (host, port, &ss)) {
		goto errparse;
	}
	if (!socket_client (&ss, SOCK_STREAM, &dianode.socket)) {
		goto errconnect;
	}
        return true;
errnode:
errconnect:
	socket_close (dianode.socket);
errparse:
	diasasl_node_close (&dianode);
errmem:
	return false;
}


/* Global Cleanup of Quick SASL.
 */
void qsasl_fini (void) {
	diasasl_node_close (&dianode);
	socket_close (dianode.socket);
}


/* Create a Client Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_CLIENTFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 */
bool qsasl_client (QuickSASL *out_qsasl,
		const char *service,
                void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate),
		void *cbdata,
		unsigned _flags) {
	log_debug ("qsasl_client() on a proxy?!?");
	errno = ENOSYS;
	*out_qsasl = NULL;
	return false;
}


/* Create a Server Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_SERVERFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 *
 * This routine differs from qsasl_client() only in
 * that it sets the distinguishing flag is_server.
 *
 * Prior to this call, *out_qsasl must be NULL.
 */
bool qsasl_server (QuickSASL *out_qsasl,
		const char *service,
                void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate),
		void *cbdata,
		unsigned flags) {
	//
	// First create zeroed storage space for the QuickSASL object
	*out_qsasl = NULL;
	if (!mem_open (sizeof (struct qsaslt_connection), (void **) out_qsasl)) {
		/* errno is set to ENOMEM */
		return false;
	}
	//
	// Take note of any flags
	if (flags & QSA_SERVERFLAG_ALLOW_FINAL_S2C) {
		(*out_qsasl)->allow_final_s2c = true;
	}
	//
	// Take note of any settings
	(*out_qsasl)->opt_cb  = opt_cb;
	(*out_qsasl)->cbdata  = cbdata;
	//
	// Return success
	return true;
}


/* Close the Client/Server Connection Object.
 *
 * This frees the SASL connection handle, plus
 * any data that was allocated in its pool.
 *
 * After this call, *inout_qsasl will be NULL.
 */
void qsasl_close (QuickSASL *inout_qsasl) {
	//
	// Trivially end when already closed
	if (*inout_qsasl == NULL) {
		return;
	}
	//
	// Close the DiaSASL session
	if ((*inout_qsasl)->opened) {
		diasasl_close (&dianode, &(*inout_qsasl)->diases);
	}
	//
	// Return session memory to the heap
	mem_close ((void **) inout_qsasl);
}


/* Internal.  Callback routine for _qsasl_have() below.
 */
static void _diasasl_cb_open ( struct diasasl_session *session,
			const int32_t *com_errno,
			char *opt_mechanism_list,
			uint8_t *s2c_token, uint32_t s2c_token_len) {
	QuickSASL qsasl = (QuickSASL) session;
	qsasl->mech_list = opt_mechanism_list;
	if (com_errno != NULL) {
		qsaslt_state cbstate = QSA_UNDECIDED;
		if (*com_errno == 0) {
			qsasl->succeeded = true;
			cbstate = QSA_SUCCESS;
		} else {
			//TODO// Report com_errno code on logs
			qsasl->failed = true;
			cbstate = QSA_FAILURE;
		}
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, cbstate);
		}
	}
}


/* Internal.  Open the connection if not done yet.
 */
static bool _qsasl_have (QuickSASL qsasl) {
	//
	// Return quickly when we have a DiaSASL session
	if (qsasl->opened) {
		goto done;
	}
	//
	// We need the service realm before we can open
	if (qsasl->srealm == NULL) {
		log_debug("Call qsasl_set_server_realm() before using the DiaSASL session");
		goto done;
	}
	//
	// Setup a callback and request to open the session
	qsasl->diases.callback = _diasasl_cb_open;
	diasasl_open (&dianode, &qsasl->diases, qsasl->srealm, NULL);
	if (diasasl_process (&dianode, true) != 0) {
		return false;
	}
	//
	// We have opened the DiaSASL session
	qsasl->opened = true;
	//
	// Return true on succes, false on failure
done:
	return qsasl->opened;
}

/* Get the Channel Binding value.
 */
bool qsasl_get_chanbind_value (QuickSASL qsasl, membuf *value) {
	errno = ENOSYS;
	return false;
}


/* Get the Channel Binding name.
 */
bool qsasl_get_chanbind_name (QuickSASL qsasl, const char **name) {
	errno = ENOSYS;
	return false;
}


/* Set the Client Identity (for SASL EXTERNAL).
 */
bool qsasl_set_clientid_external (QuickSASL qsasl, membuf clientid) {
	log_debug ("qsasl_set_clientuser_external() on a proxy -- may make sense, but not implemented");
	errno = ENOSYS;
	return false;
}


/* Set the Client username for authentication (the "login user").
 */
bool qsasl_set_clientuser_login (QuickSASL qsasl, membuf user_login) {
	log_debug ("qsasl_set_clientuser_login() on a proxy?!?");
	errno = ENOSYS;
	return false;
}


/* Set the Client username to act for (the "ACL user").
 */
bool qsasl_set_clientuser_acl (QuickSASL qsasl, membuf user_acl) {
	log_debug ("qsasl_set_clientuser_acl() on a proxy?!?");
	errno = ENOSYS;
	return false;
}


/* Get the Client UserID, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return bufptr==NULL,
 * because no client identity has been established.
 */
bool qsasl_get_clientid (QuickSASL qsasl, membuf *out_userid) {
	//
	// Set an empty response and return it in special cases
	memset (out_userid, 0, sizeof (membuf));
	if (qsasl->mech_choice == NULL) {
		/* No mechanism determined yet */
		return false;
	}
	if (!qsasl->succeeded) {
		return false;
	}
	if (strcmp (qsasl->mech_choice, "ANONYMOUS") == 0) {
		/* One mechanism, but no authenticated client identity */
		return true;
	}
	//
	// Return the client user, as determined by the protocol
	out_userid->bufptr = (uint8_t *)qsasl->diases.client_userid ;
	out_userid->buflen = strlen (qsasl->diases.client_userid);
	return true;
}


/* Get the Client Domain, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return the approving realm.
 * even though no client UserID has been established.
 */
bool qsasl_get_client_domain (QuickSASL qsasl, membuf *out_domain) {
	//
	// Set an empty response and return it in special cases
	memset (out_domain, 0, sizeof (membuf));
	if (qsasl->mech_choice == NULL) {
		/* No mechanism determined yet */
		return false;
	}
	//
	// Return the client user, as determined by the protocol
	out_domain->bufptr = (uint8_t *)qsasl->diases.client_domain ;
	out_domain->buflen = strlen (qsasl->diases.client_domain);
	return true;
}


/* Some mechanisms may delegate a credential to the
 * client.  This can be retrieved with this call.
 * The call sets DER NULL if no credential was
 * presented, but still returns true.
 */
bool qsasl_get_delegated_credential (QuickSASL qsasl, membuf *out_cred) {
	log_debug ("qsasl_get_delegated_credential() on a proxy?!?");
	errno = EINVAL;
	return false;
}


/* Set the Server Realm.
 */
bool qsasl_set_server_realm (QuickSASL qsasl, membuf srealm) {
	assert (srealm.bufptr != NULL);
	assert (qsasl->srealm == NULL);
	log_debug ("Setting DiaSASL srealm=%.*s in %p", (int)srealm.buflen, srealm.bufptr, qsasl);
	return mem_strdup (qsasl, srealm, &qsasl->srealm);
}


/* Set the Client Realm.
 */
bool qsasl_set_client_realm (QuickSASL qsasl, membuf crealm) {
	log_debug ("qsasl_set_client_realm() on a proxy?!?");
	errno = ENOSYS;
	return false;
}


/* Get the Mechanism(s) that is/are currently possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool qsasl_get_mech (QuickSASL qsasl, membuf *out_mech) {
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	assert (qsasl->mech_list != NULL);
	log_debug ("qsasl_get_mech () --> %s", qsasl->mech_list);
	out_mech->bufptr = (uint8_t *)qsasl->mech_list ;
	out_mech->buflen = strlen (qsasl->mech_list);
	return true;
}


/* Set the Mechanism(s) to be considered possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 *
 * In DiaSASL, this is used differently; we use this
 * to set the client choice.  This is normal for the
 * server side of a SASL exchange, and that is the
 * only thing that DiaSASL offers.
 */
bool qsasl_set_mech (QuickSASL qsasl, membuf mech) {
	qsasl->mech_choice = NULL;
	return mem_strdup (qsasl, mech, &qsasl->mech_choice);
}


/* Determine the flags for success or failure.  They may
 * both be reset, in which case the work is not done.
 * The return value indicates success of the call that
 * fills the flags.
 */
bool qsasl_get_outcome (QuickSASL qsasl, bool *success, bool *failure) {
	assert (success != NULL);
	assert (failure != NULL);
	if (qsasl->succeeded && qsasl->failed) {
		return false;
	}
	*success = qsasl->succeeded;
	*failure = qsasl->failed;
	return true;
}


/* Make a step as a SASL Client.
 */
bool qsasl_step_client (QuickSASL qsasl, membuf s2c, membuf *out_mech, membuf *out_c2s) {
	log_debug ("qsasl_step_client() on a proxy?!?");
	errno = ENOSYS;
	return false;
}


/* Internal.  Callback routine for qsasl_step_server() below.
 */
static void _diasasl_cb_step ( struct diasasl_session *session,
			const int32_t *com_errno,
			char *opt_mechanism_list,
			uint8_t *s2c_token, uint32_t s2c_token_len) {
	QuickSASL qsasl = (QuickSASL) session;
	qsasl->s2c_token->bufptr = s2c_token;
	qsasl->s2c_token->buflen = s2c_token_len;
	if (com_errno != NULL) {
		qsaslt_state cbstate = QSA_UNDECIDED;
		if (*com_errno == 0) {
			qsasl->succeeded = true;
			cbstate = QSA_SUCCESS;
		} else {
			//TODO// Report com_errno code on logs
			qsasl->failed = true;
			cbstate = QSA_FAILURE;
		}
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, cbstate);
		}
	}
}


/* Make a step as a SASL Server.
 */
bool qsasl_step_server (QuickSASL qsasl, membuf c2s, membuf *out_s2c) {
	//
	// If not done yet, open the DiaSASL session
	if (!_qsasl_have (qsasl)) {
		return false;
	}
	if (qsasl->failed) {
		errno = EPIPE;
		return false;
	}
	//
	// By now, we should have a mechanism choice
	char *opt_mechanism_choice = NULL;
	if (!qsasl->started) {
		assert (qsasl->mech_choice != NULL);
		opt_mechanism_choice = qsasl->mech_choice;
		qsasl->started = true;
	}
	//
	// Setup the callback and send a DiaSASL message
	qsasl->s2c_token = (void *) out_s2c;
	qsasl->diases.callback = _diasasl_cb_step;
	diasasl_send (&dianode,
			&qsasl->diases,
			opt_mechanism_choice,
			c2s.bufptr,
			c2s.buflen);
	//
	// Make an attempt to read one response.
	//TODO//CONCURRENCY_SUPPORT_NEEDED//
	// This currently assumes a non-shared and
	// blocking socket.  That is not practical
	// in massive server setups, and we should
	// consider other options, such as running
	// a background master thread and ensuring
	// that not two processes could end up
	// sharing a socket.
	int32_t com_errno = diasasl_process (&dianode, true);
	if (com_errno != 0) {
		//TODO// Consider logging com_errno
		errno = com_errno;
		return false;
	}
	//
	// We were successful, even if that may only be
	// a partial result on non-blocking sockets.
	return true;
}

