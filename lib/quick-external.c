/* Isolated EXTERNAL implementation of Quick SASL.
 *
 * This assumes that context (usually the X.509 connection)
 * has established a client identity and that this can be
 * claimed by the client.  The client can provide a desired
 * authorisation/acl identity, which must then be available
 * to the authentication/login identity.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <errno.h>

#include <arpa2/identity.h>
#include <arpa2/access_actor.h>
#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>
#include <arpa2/except.h>

#include "sasl-dbg.h"


struct qsaslt_connection {
	const char *service;
	char *srealm;
	char *crealm;
	char *user_login;
	char *user_acl;
	char *postmech_user;
	char *mechlist;
	void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate);
	void *cbdata;
	bool is_server;
	bool allow_final_s2c;
	bool started;
	bool succeeded;
	bool failed;
};


#if 0
/* improved callback to verify authorization;
 *     canonicalization now handled elsewhere
 *  conn           -- connection context
 *  requested_user -- the identity/username to authorize (NUL terminated)
 *  rlen           -- length of requested_user
 *  auth_identity  -- the identity associated with the secret (NUL terminated)
 *  alen           -- length of auth_identity
 *  default_realm  -- default user realm, as passed to sasl_server_new if
 *  urlen          -- length of default realm
 *  propctx        -- auxiliary properties
 * returns SASL_OK on success,
 *         SASL_NOAUTHZ or other SASL response on failure
 */
int arpa2_acluser (sasl_conn_t *conn, void *context,
			const char *requested_user, unsigned rlen,
			const char *auth_identity, unsigned alen,
			const char *def_realm, unsigned urlen,
			struct propctx *propctx) {
	log_debug ("Matching user_acl=%.*s#%d to user_login=%.*s#%d in realm %.*s",
			rlen, requested_user, rlen, alen, auth_identity, alen, urlen, def_realm);
	//
	// Try to parse both identifiers as ARPA2 Identity
	// Question: Is it an improvement to attach realm to auth_identity?
	// Question: Is it an improvement to allow local-part in requested_user?
	// Answers:  For now, we'll demand users to provide user@domain.name!
	a2id_t current_id;
	a2id_t desired_id;
	bool bothok = true;
	bothok = bothok && a2id_parse (&current_id, auth_identity,  alen);
	bothok = bothok && a2id_parse (&desired_id, requested_user, rlen);
	//
	// Simplicity fallback: Names must be equal, modulo case.
	//                      No fancy treatment without ARPA2.
	if (!bothok) {
		if (strcasecmp (requested_user, auth_identity) == 0) {
			return SASL_OK;
		} else {
			return SASL_NOAUTHZ;
		}
	}
	//
	// Given a realm, we should use ARPA2 Identity and ARPA2 Access Control
	// and more specifically, the access_actor() function to test for
	// the ability to switch to a desired Actor Identity
	if (access_actor (&current_id, &desired_id)) {
		return SASL_OK;
	} else {
		return SASL_NOAUTHZ;
	}
}
#endif


#if 0
/* logging callback -- this allows plugins and the middleware to
 *  log operations they perform.
 * inputs:
 *  context     -- logging context from the callback record
 *  level       -- logging level; see above
 *  message     -- message to log
 * returns:
 *  SASL_OK     -- no error
 *  SASL_FAIL   -- error
 */
int arpa2_logging (void *context, int level, const char *message) {
	switch (level) {
	case SASL_LOG_ERR:
	case SASL_LOG_FAIL:
		log_error ("%s", message);
		break;
	case SASL_LOG_WARN:
		log_warning ("%s", message);
		break;
	case SASL_LOG_NOTE:
		log_notice ("%s", message);
		break;
	case SASL_LOG_DEBUG:
	case SASL_LOG_TRACE:
		log_debug ("%s", message);
		break;
	case SASL_LOG_NONE:
	case SASL_LOG_PASS:
	default:
		break;
	}
	return 0;
}
#endif


#if 0
/* callback for a server-supplied user canonicalization function.
 *
 * This function is called directly after the mechanism has the
 * authentication and authorization IDs.  It is called before any
 * User Canonicalization plugin is called.  It has the responsibility
 * of copying its output into the provided output buffers.
 *
 *  in, inlen     -- user name to canonicalize, may not be NUL terminated
 *                   may be same buffer as out
 *  flags         -- not currently used, supplied by auth mechanism
 *  user_realm    -- the user realm (may be NULL in case of client)
 *  out           -- buffer to copy user name
 *  out_max       -- max length of user name
 *  out_len       -- set to length of user name
 *
 * returns
 *  SASL_OK         on success
 *  SASL_BADPROT    username contains invalid character
 */
int arpa2_canon_user (sasl_conn_t *conn,
                              void *context,
                              const char *in, unsigned inlen,
                              unsigned flags,
                              const char *user_realm,
                              char *out,
                              unsigned out_max, unsigned *out_len) {
	char fulluser [inlen + 1 + strlen (user_realm) + 1];
	memcpy (fulluser, in, inlen);
	fulluser [inlen] = '\0';
	if (memchr (fulluser, '@', inlen) == NULL) {
		fulluser [inlen] = '@';
		strcpy (fulluser + inlen + 1, user_realm);
	}
	a2id_t canonuser;
	if (!a2id_parse (&canonuser, fulluser, 0)) {
		return SASL_BADPROT;
	}
	int canonlen = canonuser.ofs [A2ID_OFS_END];
	if (canonlen > out_max) {
		return SASL_NOMEM;
	}
	memcpy (out, canonuser.txt, canonlen);
	*out_len = canonlen;
	return SASL_OK;
}
#endif



/* Global Initialisation of Quick SASL.
 *
 * If you need to provide a driver library hint,
 * this is where to do it.  Not actually used yet.
 *
 * The default appname is "Hosted_Identity".
 */
bool qsasl_init (const char *_opt_driver, const char *appname) {
	if (appname == NULL) {
		appname = "Hosted_Identity";
	}
        return true;
}


/* Global Cleanup of Quick SASL.
 */
void qsasl_fini (void) {
        ;
}


/* Create a Client Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_CLIENTFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 */
bool qsasl_client (QuickSASL *out_qsasl,
		const char *service,
                void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate),
		void *cbdata,
		unsigned _flags) {
	if (!mem_open (sizeof (struct qsaslt_connection), (void **) out_qsasl)) {
		/* errno is set to ENOMEM */
		return false;
	}
	(*out_qsasl)->opt_cb  = opt_cb;
	(*out_qsasl)->cbdata  = cbdata;
	(*out_qsasl)->service = service;
	log_debug ("qsasl_client at %p..%p", *out_qsasl, ((uint8_t *) *out_qsasl) + sizeof (struct qsaslt_connection) -1);
	// Note: No meaningful flags at this time
	return true;
}


/* Create a Server Connection Object.
 *
 * Optionally provide a callback function that will
 * be called when the state changes to QSA_SUCCESS
 * or QSA_FAILURE.
 *
 * The flags may be set to QSA_SERVERFLAG_xxx
 *
 * This routine allocates a mem_pool containing
 * itself.  The mem_alloc() function can be used
 * to add more data to its pool.
 *
 * This routine differs from qsasl_client() only in
 * that it sets the distinguishing flag is_server.
 *
 * Prior to this call, *out_qsasl must be NULL.
 */
bool qsasl_server (QuickSASL *out_qsasl,
		const char *service,
                void (*opt_cb) (void *cbdata, QuickSASL qsasl, enum qsaslt_state newstate),
		void *cbdata,
		unsigned flags) {
	if (!qsasl_client (out_qsasl, service, opt_cb, cbdata, 0)) {
		return false;
	}
	(*out_qsasl)->is_server = true;
	if (flags & QSA_SERVERFLAG_ALLOW_FINAL_S2C) {
		(*out_qsasl)->allow_final_s2c = true;
	}
	return true;
}


/* Close the Client/Server Connection Object.
 *
 * This frees any data that was allocated in the pool.
 *
 * After this call, *inout_qsasl will be NULL.
 */
void qsasl_close (QuickSASL *inout_qsasl) {
	mem_close ((void **) inout_qsasl);
}


/* Set the Channel Binding directly
 * This is not supported for the EXTERNAL mechanism.
 */
bool qsasl_set_chanbind (QuickSASL qsasl, bool enforce, membuf value) {
	return false;
}


/* Get the Channel Binding value.
 * This is not supported for the EXTERNAL mechanism.
 */
bool qsasl_get_chanbind_value (QuickSASL qsasl, membuf *value) {
	value->bufptr = NULL;
	value->buflen = 0;
	return false;
}


/* Get the Channel Binding name.
 * This is not supported for the EXTERNAL mechanism.
 */
bool qsasl_get_chanbind_name (QuickSASL qsasl, const char **name) {
	*name = NULL;
	return false;
}


/* Set the Client Identity (for SASL EXTERNAL).
 */
bool qsasl_set_clientid_external (QuickSASL qsasl, membuf clientid) {
	assert (clientid.bufptr != NULL);
	if (!mem_strdup (qsasl, clientid, &qsasl->user_login)) {
		/* errno is set to ENOMEM */
		return false;
	}
	return true;
}


/* Set the Client username for authentication (the "login user").
 */
bool qsasl_set_clientuser_login (QuickSASL qsasl, membuf user_login) {
	assert (user_login.bufptr != NULL);
	assert (qsasl->user_login == NULL);
	log_debug ("Setting QuickSASL user_login=%.*s in %p", (int)user_login.buflen, user_login.bufptr, qsasl);
	/* errno will be set to ENOMEM on failure */
	return mem_strdup (qsasl, user_login, &qsasl->user_login);
}


/* Set the Client username to act for (the "ACL user").
 */
bool qsasl_set_clientuser_acl (QuickSASL qsasl, membuf user_acl) {
	assert (user_acl.bufptr != NULL);
	assert (qsasl->user_acl == NULL);
	log_debug ("Setting QuickSASL user_acl=%.*s in %p", (int)user_acl.buflen, user_acl.bufptr, qsasl);
	/* errno will be set to ENOMEM on failure */
	return mem_strdup (qsasl, user_acl, &qsasl->user_acl);
}


/* Following is client userid/domain fetching code.  This is made
 * to unravel the user@realm code above, which follows what is
 * stated in <sasl/sasl.h>:
 *
 *************************************************
 * IMPORTANT NOTE: server realms / username syntax
 *
 * If a user name contains a "@", then the rightmost "@" in the user name
 * separates the account name from the realm in which this account is
 * located.  A single server may support multiple realms.  If the
 * server knows the realm at connection creation time (e.g., a server
 * with multiple IP addresses tightly binds one address to a specific
 * realm) then that realm must be passed in the user_realm field of
 * the sasl_server_new call.  If user_realm is non-empty and an
 * unqualified user name is supplied, then the canon_user facility is
 * expected to append "@" and user_realm to the user name.  The canon_user
 * facility may treat other characters such as "%" as equivalent to "@".
 *
 * If the server forbids the use of "@" in user names for other
 * purposes, this simplifies security validation.
 */


/* Have the post-mechanism client identity in user[@realm] form.
 * Return true when the string should be processed, rather than a
 * failure of the call.  The string is explicitly provided as
 * absent for the exceptional case of ANONYMOUS "authentication".
 */
static bool _have_postmech_user (QuickSASL qsasl) {
	if (qsasl->postmech_user != NULL) {
		return true;
	}
	if (qsasl->mechlist == NULL) {
		/* No mechanism determined yet */
		log_debug ("No post-mechanism user due to missing mechanism list");
		return false;
	}
	if (strchr (qsasl->mechlist, ' ') != NULL) {
		/* Mechanism list not focussed on one yet */
		log_debug ("No post-mechanism user due to missing mechanism choice");
		return false;
	}
	if (strcmp (qsasl->mechlist, "ANONYMOUS") == 0) {
		/* One mechanism, but no authenticated client identity */
		return true;
	}
	qsasl->postmech_user = qsasl->user_acl;
	return (qsasl->postmech_user != NULL);
}


/* Get the Client UserID, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this would return bufptr==NULL,
 * because no client identity has been established.
 */
bool qsasl_get_client_userid (QuickSASL qsasl, membuf *out_userid) {
	memset (out_userid, 0, sizeof (membuf));
	if (!_have_postmech_user (qsasl)) {
		return false;
	}
	out_userid->bufptr = (uint8_t*)qsasl->postmech_user;
	out_userid->buflen = (qsasl->postmech_user) ? strlen (qsasl->postmech_user) : 0;
	if (qsasl->postmech_user == NULL) {
		/* ANONYMOUS successfully reports NULL */
		log_debug ("ANONYMOUS authentication reports NULL as userid");
		return true;
	}
	int at = out_userid->buflen;
	while (at-- > 0) {
		if (out_userid->bufptr [at] == '@') {
			out_userid->buflen = at;
			break;
		}
	}
	log_debug ("qsasl_get_client_userid() -> \"%.*s[%s]\"", at, qsasl->postmech_user, &qsasl->postmech_user [at]);
	return true;
}


/* Get the Client Domain, available after success.
 * The result has been subjected to SASL security.
 * For ANONYMOUS, this may return the realm of the
 * server if one was defined, even though no UserID
 * has been proven.
 */
bool qsasl_get_client_domain (QuickSASL qsasl, membuf *out_domain) {
	memset (out_domain, 0, sizeof (membuf));
	if (!_have_postmech_user (qsasl)) {
		return false;
	}
	if (qsasl->postmech_user == NULL) {
		/* ANONYMOUS reports the server domain */
		if (qsasl->srealm != NULL) {
			log_debug ("ANONYMOUS authentication reports the domain %s", qsasl->srealm);
			out_domain->bufptr = (uint8_t*)qsasl->srealm ;
			out_domain->buflen = strlen (qsasl->srealm);
		}
		return true;
	}
	int at = strlen (qsasl->postmech_user);
	while (at-- > 0) {
		if (qsasl->postmech_user [at] == '@') {
			break;
		}
	}
	char* domain_part = &qsasl->postmech_user [at+1];
	out_domain->bufptr = (uint8_t*)domain_part;
	out_domain->buflen = strlen (domain_part);
	log_debug ("qsasl_get_client_domain() -> \"[%.*s]%s\"", at+1, qsasl->postmech_user, out_domain->bufptr);
	return true;
}


/* Some mechanisms may delegate a credential to the
 * client.  This can be retrieved with this call.
 * The call sets DER NULL if no credential was
 * presented, but still returns true.
 * This is not supported by the EXTERNAL mechanism.
 */
bool qsasl_get_delegated_credential (QuickSASL qsasl, membuf *out_cred) {
	static uint8_t der_null [] = { 0x05, 0x00 };
	out_cred->bufptr = NULL;
	out_cred->buflen = 0;
	return true;
}


/* Set the Server Realm.
 */
bool qsasl_set_server_realm (QuickSASL qsasl, membuf srealm) {
	assert (srealm.bufptr != NULL);
	assert (qsasl->srealm == NULL);
	log_debug ("Setting QuickSASL srealm=%.*s in %p", (int)srealm.buflen, srealm.bufptr, qsasl);
	return mem_strdup (qsasl, srealm, &qsasl->srealm);
}


/* Set the Client Realm.
 */
bool qsasl_set_client_realm (QuickSASL qsasl, membuf crealm) {
	assert (crealm.bufptr != NULL);
	assert (qsasl->crealm == NULL);
	log_debug ("Setting QuickSASL crealm=%.*s in %p", (int)crealm.buflen, crealm.bufptr, qsasl);
	return mem_strdup (qsasl, crealm, &qsasl->crealm);
}


/* Get the Mechanism(s) that is/are currently possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
static const char mech_EXTERNAL [] = "EXTERNAL";
static const char mech_none [] = "";
bool qsasl_get_mech (QuickSASL qsasl, membuf *out_mech) {
	assert (qsasl->is_server);
	if (qsasl->user_login != NULL) {
		out_mech->bufptr = (uint8_t *) mech_EXTERNAL;
		out_mech->buflen = sizeof (mech_EXTERNAL) - 1;
	} else {
		out_mech->bufptr = (uint8_t *) mech_none;
		out_mech->buflen = sizeof (mech_none) - 1;
	}
	return true;
}


/* Fallback mechanism list, used when a client or server
 * is started without a mechanism having been set.
 */
const char *mechlist_fallback_anonymous = "ANONYMOUS";


/* Set the Mechanism(s) to be considered possible.
 *
 * Before the client chooses, there may be any number
 * of possible mechanisms; after the choice there is
 * only one.  In both cases, a single string is used to
 * represent the mechanism(s), with spaces to separate
 * multiple options.
 */
bool qsasl_set_mech (QuickSASL qsasl, membuf mech) {
	if (mech.buflen != sizeof (mech_EXTERNAL) - 1) {
		return false;
	}
	if (0 != memcmp (mech.bufptr, mech_EXTERNAL, mech.buflen)) {
		return false;
	}
	qsasl->mechlist = (char *) mech_EXTERNAL;
}

/* Determine the flags for success or failure.  They may
 * both be reset, in which case the work is not done.
 * The return value indicates success of the call that
 * fills the flags.
 */
bool qsasl_get_outcome (QuickSASL qsasl, bool *success, bool *failure) {
	assert (success != NULL);
	assert (failure != NULL);
	if (qsasl->succeeded && qsasl->failed) {
		return false;
	}
	*success = qsasl->succeeded;
	*failure = qsasl->failed;
	return true;
}

/* Make a step as a SASL Client.
 */
bool qsasl_step_client (QuickSASL qsasl, membuf s2c, membuf *out_mech, membuf *out_c2s) {
	//
	// Initialisation with sanity checking
	memset (out_mech, 0, sizeof (membuf));
	memset (out_c2s,  0, sizeof (membuf));
	//
	// Avoid continuous sending to an inquisitive server
	if (qsasl->started) {
		log_debug ("qsasl_step_client() seems to be looping forever, reporting failure");
		assert (!qsasl->succeeded);
		assert (!qsasl->failed);
		qsasl->failed = true;
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl, QSA_FAILURE);
			qsasl->opt_cb = NULL;
		}
		return true;
	}
	//
	// Send the mechanism name
	out_mech->bufptr = (char *) mech_EXTERNAL;
	out_mech->buflen = sizeof (mech_EXTERNAL) - 1;
	//
	// Send a username or otherwise an empty string
	if (qsasl->user_acl != NULL) {
		out_c2s->bufptr = qsasl->user_acl;
	} else if (qsasl->user_login != NULL) {
		out_c2s->bufptr = qsasl->user_login;
	} else {
		out_c2s->bufptr = "";
	}
	out_c2s->buflen = strlen (out_c2s->bufptr);
	log_debug ("sasl_client_step() sends \"%s\"", out_c2s->bufptr);
	//
	// Avoid repeating sends
	qsasl->started = true;
	return true;
}


/* Make a step as a SASL Server.
 */
bool qsasl_step_server (QuickSASL qsasl, membuf c2s, membuf *out_s2c) {
	//
	// Initialisation with sanity checking
	memset (out_s2c, 0, sizeof (membuf));
	const char *srvout    = NULL;
	unsigned    srvoutlen = 0;
	bool failure = false;
	bool success = false;
	//
	// If the client did not send a token, the server must
	// challenge it with an empty token
	if (c2s.bufptr == NULL) {
		out_s2c->bufptr = "";
		log_debug ("qsasl_step_server() got no client token, and sends an empty-string nudge");
	}
	//
	// If the client identity is unknown, fail authentication
	else if (qsasl->user_login == NULL) {
		success = false;
		log_debug ("qsasl_step_server() has no external client identity, and fails");
	}
	//
	// If the client sent an empty string, accept with the
	// authentication user
	else if (c2s.buflen == 0) {
		membuf user = {
			.bufptr = qsasl->user_login,
			.buflen = strlen (qsasl->user_login)
		};
		mem_strdup (qsasl, user, &qsasl->user_acl);
		success = true;
		log_debug ("qsasl_step_server() will trust the client idenity");
	}
	//
	// Otherwise, treat c2s as an authorisation identity request
	else {
		a2id_t current_id;
		a2id_t desired_id;
		if (!a2id_parse (&current_id, qsasl->user_login, 0)) {
			log_debug ("qsasl_step_server() failed to parse current identity \"%s\"", qsasl->user_login);
			failure = true;
		}
		if (!a2id_parse (&desired_id, c2s.bufptr, c2s.buflen)) {
			log_debug ("qsasl_step_server() failed to parse desired identity \"%.*s\"", c2s.buflen, c2s.bufptr);
			failure = true;
		}
		if (!failure) {
			success = access_actor (&current_id, &desired_id);
			failure = !success;
		}
		log_debug ("qsasl_step_server() tried the client idenity (success %d, failure %d)", success, failure);
	}
	//
	// Possibly report back, depending on success or failure
	if (failure || success) {
		assert (!qsasl->succeeded);
		assert (!qsasl->failed);
		assert (! (success && failure));
		qsasl->failed    = failure;
		qsasl->succeeded = success;
		if (qsasl->opt_cb != NULL) {
			qsasl->opt_cb (qsasl->cbdata, qsasl,
					success ? QSA_SUCCESS : QSA_FAILURE);
			qsasl->opt_cb = NULL;
		}
	}
	//
	// Return technical success (not necessarily authentication success)
	return true;
}
