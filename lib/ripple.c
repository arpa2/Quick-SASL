/* Ripple List Management, using spin locks.
 *
 * These are singly-linked lists with references in
 * head and next fields.  The references hold an extra
 * spin lock which protects the reference and the data
 * that is being referenced by it.
 *
 * The rippling routing locks elements in sequence,
 * and can invoke an action when 1 or 2 are locked.
 * This can be used to construct semantics such as:
 *  - at 1: Find and process a single element
 *  - at 1: Add a new element after a locked one
 *  - at 2: Remove an element from the list
 *
 * A newly added element needs no lock; a removed
 * element will be unlocked after its removal.
 *
 * Actions may find NULL in their last reference.
 * Action are called with a callback data pointer.
 * Each routine returns whether the chase continues.
 *
 * When the symbol RIPPLE_WITHOUT_SPINLOCKS exists,
 * the spinlocks are suppressed in resulting code.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <assert.h>

#include <arpa2/except.h>
#include <arpa2/ripple.h>


#ifndef RIPPLE_WITHOUT_SPINLOCKS
# define SPINLOCK(rr)  assert (0 == pthread_spin_lock    (&(rr)->lock))
# define SPUNLOCK(rr)  assert (0 == pthread_spin_unlock  (&(rr)->lock))
# define SPIN_INIT(rr) assert (0 == pthread_spin_init    (&(rr)->lock, PTHREAD_PROCESS_SHARED))
# define SPIN_FINI(rr) assert (0 == pthread_spin_destroy (&(rr)->lock))
#else
# define SPINLOCK(rr)
# define SPUNLOCK(rr)
# define SPIN_INIT(rr)
# define SPIN_FINI(rr)
#endif



/* Initialise a list head as an empty list.
 */
void ripple_init (ripple_ref *head) {
	assert (head != NULL);
	head->ref = NULL;
	SPIN_INIT (head);
}


/* Cleanup a list head.
 */
void ripple_fini (ripple_ref *head) {
	assert (head != NULL);
	SPIN_FINI (head);
}


/* Ripple iterator.  Start at the head and pass through
 * the list for as long as callbacks return true to
 * continue.  At the end, return the result from the last
 * callback.  The head must not be NULL.
 *
 * The callbacks are cb1 and cb2; the former is used with
 * just one element locked and non-NULL, the latter is
 * used when the next element is also locked and non-NULL.
 * The cbdata is supplied to both types of callback and
 * may serve as an accumulator.
 *
 * When skip1st is true, the head is not passed into the
 * cb1 callback.  This is necessary when cb1 interprets
 * the structure of the head; cb2 is assumed to only be
 * interested in the ripple_ref structure of the first.
 *
 * Callbacks may read and write only what they have been
 * given with a lock.  The ripple_ref are free to change
 * within callbacks, but all prior or later elements must
 * remain in the list, in their current positions, to
 * allow other processes to ripple through.
 */
bool ripple_iter (ripple_ref *head, bool skip1st, ripple_cb *cb1, ripple_cb *cb2, void *cbdata) {
	assert (head != NULL);
	ripple_cb *cb1a = skip1st ? NULL : cb1;
	// head is            not NULL
	SPINLOCK (head);
	// head is locked and not NULL
	ripple_ref *next = head->ref;
	// head is locked and not NULL
	// next           may be  NULL
	bool cont = true;
	while (cont) {
		// head is locked and not NULL
		// next           may be  NULL
		if (cb1a != NULL) {
			cont = cb1a (head, cbdata);
		} else {
			cb1a = cb1;
		}
		// head is locked and not NULL
		// next           may be  NULL
		if (next == NULL) {
			break;
		} else if (cont) {
			// head is locked and not NULL
			// next is            not NULL
			SPINLOCK (next);
			// head is locked and not NULL
			// next is locked and not NULL
			if (cb2 != NULL) {
				cont = cb2 (head, cbdata);
				/* Was head->next changed?  In that case,
				 * try again with the same head and the
				 * new head->next as the next value.
				 */
				if (next != head->ref) {
					// head is locked and not NULL
					// next is locked and not NULL
					SPUNLOCK (next);
					// head is locked and not NULL
					// next is            not NULL
					next = head->ref;
					// head is locked and not NULL
					// next            may be NULL
					continue;
				}
			}
			// head is locked and not NULL
			// next is locked and not NULL
			SPUNLOCK (head);
			// next is locked and not NULL
			head = next;
			// head is locked and not NULL
			next = head->ref;
			// head is locked and not NULL
			// next           may be  NULL
		}
	}
	// head is locked and not NULL
	SPUNLOCK (head);
	// head is not NULL
	return cont;
}


/* Insert a new item at the start of a ripple list.
 * The new item does not have to be list-initialised.
 *
 * This function does not fail.
 */
void ripple_insert (ripple_ref *head, ripple_ref *newbe) {
	SPIN_INIT (newbe);
	SPINLOCK (head);
	newbe->ref = head->ref;
	head->ref = newbe;
	SPUNLOCK (head);
}


/* Append a new item to the end of a ripple list.
 * The new item does not have to be list-initialised.
 *
 * This function does not fail.
 */
static bool _ripple_cb1_append (ripple_ref *maybe_last, void *cbdata) {
	log_debug ("_ripple_cb1_append() attempts %p", maybe_last);
	if (maybe_last->ref != NULL) {
		return true;
	}
	ripple_ref *newbe = cbdata;
	SPIN_INIT (newbe);
	newbe->ref = NULL;
	maybe_last->ref = newbe;
	log_debug ("_ripple_cb1_append() added %p after %p", newbe, maybe_last);
	return false;
}
//
void ripple_append (ripple_ref *head, ripple_ref *newbe) {
	ripple_iter (head, false, _ripple_cb1_append, NULL, newbe);
}


/* Remove an element from a ripple list.
 *
 * Return whether the element was found and removed.
 */
static bool _ripple_cb2_remove (ripple_ref *maybe_previous, void *cbdata) {
	log_debug ("_ripple_cb2_remove() tries %p->ref for removal of %p (cbdata %p)", maybe_previous, maybe_previous->ref, cbdata);
	if (maybe_previous->ref != cbdata) {
		return true;
	}
	log_debug ("_ripple_cb2_remove() replaces ->ref %p with ->ref->ref %p", maybe_previous->ref, maybe_previous->ref->ref);
	ripple_ref *goner = cbdata;
	maybe_previous->ref = goner->ref;
	goner->ref = NULL;
	return false;
}
//
bool ripple_remove (ripple_ref *head, ripple_ref *goner) {
	if (ripple_iter (head, false, NULL, _ripple_cb2_remove, goner)) {
		// Not found, return failure
		log_debug ("ripple_remove() did not find the instance");
		return false;
	}
	// Found and removed, now cleanup the spin lock
	//BUGGY// SPIN_FINI (goner);
	log_debug ("ripple_remove() got rid of the instance");
	return true;
}


/* Find the first element in a ripple list that returns
 * true from a test function.  The test function may
 * expand the ripple_ref to a larger structure to see
 * the fields surrounding it if it can be sure that the
 * list was filled with such structures.
 */
typedef struct ripple_finder {
	ripple_test *testfun;
	ripple_ref *findings;
	void *cbdata;
} ripple_finder;
//
static bool _ripple_cb1_findit (ripple_ref *maybe, void *cbdata) {
	ripple_finder *finder = cbdata;
	log_debug ("_ripple_cb1_findit() tests %p", maybe);
	if (!finder->testfun (maybe, finder->cbdata)) {
		return true;
	}
	log_debug ("_ripple_cb1_findit() returns %p", maybe);
	finder->findings = maybe;
	return false;
}
//
struct ripple_ref *ripple_find (ripple_ref *head, ripple_test *testfun, void *cbdata) {
	struct ripple_finder finder;
	finder.testfun = testfun;
	finder.findings = NULL;
	finder.cbdata = cbdata;
	ripple_iter (head, true, _ripple_cb1_findit, NULL, &finder);
	log_debug ("ripple_find() returns %p", finder.findings);
	return finder.findings;
}


/* Map a function over a ripple list while a condition holds.
 * The callback function returns true as its while condition.
 *
 * The return value is the last value returned.
 */
bool ripple_mapwhile (ripple_ref *head, ripple_test *maptestfun, void *cbdata) {
	return ripple_iter (head, true, maptestfun, NULL, cbdata);
}


/* Drop elements from a ripple list while a condition holds.
 * The callback function removes an element and returns
 * true as its while condition.  Supply NULL as a function
 * to drop every element from the list.
 *
 * The return value is the last value returned.
 */
static bool _ripple_cb2_drop (ripple_ref *gonerptr, void *cbdata) {
	ripple_ref *goner = gonerptr->ref;
	gonerptr->ref = goner->ref;
	goner->ref = NULL;
	return true;
}
//
bool ripple_dropwhile (ripple_ref *head, ripple_test *testfun, void *cbdata) {
	return ripple_iter (head, true, testfun, _ripple_cb2_drop, cbdata);
	//TODO// did not SPIN_FINI(...dropped...)
}

