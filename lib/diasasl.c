/* Diameter SASL interface calls.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * Author: Henri Manson <info@mansoft.nl>
 * Author: Rick van Rein <rick@openfortress.nl>
 */


#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <assert.h>

#include <unistd.h>
#include <ctype.h>

#include <arpa2/socket.h>
#include <arpa2/except.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-diasasl.h>

#include <quick-der/Quick-DiaSASL.h>

typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Authn_Request SASL_DIAMETER_AAR;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Answer        SASL_DIAMETER_AAA;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Open_Request  SASL_DIAMETER_OPEN;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Close_Request SASL_DIAMETER_CLOSE;

static const derwalk pack_diameter_open[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Open_Request,
	DER_PACK_END
};

static const derwalk pack_diameter_close[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Close_Request,
	DER_PACK_END
};

static const derwalk pack_diameter_aar[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Authn_Request,
	DER_PACK_END
};

static const derwalk pack_diameter_aaa[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Answer,
	DER_PACK_END
};

static const char *sessionid_not_yet_set = "";


void diasasl_node_open (struct diasasl_node *node) {
	ripple_init (&node->_opaque.head);
}


void diasasl_node_close (struct diasasl_node *node) {
	diasasl_break (node, EPIPE);
	ripple_fini (&node->_opaque.head);
}


void diasasl_open (struct diasasl_node *node,
		struct diasasl_session *new_session,
		char *server_domain, char *opt_svcproto) {
	bool rc = false;
	assert(server_domain != NULL);
	assert(new_session->callback != NULL);
	log_debug ("DiaSASL-Open-Request for %s", server_domain);
	//
	// Prepare structural data
	new_session->_opaque.sessionid = (char *) sessionid_not_yet_set;
	new_session->_opaque.state = NULL;
	new_session->_opaque.state_len = 0;
	new_session->client_userid = NULL;
	new_session->client_domain = server_domain;
	ripple_append (&node->_opaque.head, &new_session->_opaque.next);
	//
	// Construct the DiaSASL-Open-Request to be sent
	SASL_DIAMETER_OPEN diameter_open;
	memset (&diameter_open, 0, sizeof (diameter_open));
	diameter_open.service_realm.derptr = (uint8_t *)server_domain ;
	diameter_open.service_realm.derlen = strlen (server_domain);
	/* Possibly add the service-trunk number */
	if (node->trunk_set) {
		der_buf_uint32_t der_trunk;
		diameter_open.service_trunk = der_put_uint32 (der_trunk, node->trunk);
	}
	/* Possibly add the service-proto name */
	if (opt_svcproto != NULL) {
		diameter_open.service_proto.derptr = (uint8_t *)opt_svcproto ;
		diameter_open.service_proto.derlen = strlen (opt_svcproto);
	}
	//
	// Send the DiaSASL-Open-Request
	if (dermem_send (new_session, node->socket, pack_diameter_open, (const dercursor *) &diameter_open)) {
		log_debug("dermem_send: Succes");
	} else {
		log_debug("dermem_send: FAIL!");
	}
}


/* Process any data returned from the local/trusted Diameter
 * node by invoking callbacks.  When no data is available
 * from the node, nothing will be done.  When multiple answers
 * are available, they will all be handled, except when the
 * just_one flag is set, in which case at most one is handled.
 *
 * The return value is a com_errno and is 0 for success.  If
 * an attempted read returns EAGAIN or EWOULDBLOCK, which is
 * regularly the case on a non-blocking socket, it returns
 * 0 for success, so this temporary setback is not fatal.
 * All remaining non-0 return values are basically fatal and
 * may lead to a reconnection attempt or may be handled by
 * calling diasasl_break() to stop outstanding requests.
 */
static bool _ripple_process_open_finder (ripple_ref *maybe, void *cbdata) {
	struct diasasl_session *session = (struct diasasl_session *) maybe;
	log_debug ("_ripple_process_open_finder() tries %p with sessionid '%p' and client_domain '%s'", session, session->_opaque.sessionid, session->client_domain);
	//
	// Skip sessions that already have a session_id
	if (session->_opaque.sessionid != sessionid_not_yet_set) {
		return false;
	}
	//
	// Skip sessions with a client_domain different from the opened realm
	DER_OVLY_Quick_DiaSASL_DiaSASL_Open_Answer *open_answer = cbdata;
	log_debug ("_ripple_process_open_finder() is looking for realm '%.*s'", (int)open_answer->service_realm.derlen, open_answer->service_realm.derptr);
	if (memcmp (open_answer->service_realm.derptr, session->client_domain, open_answer->service_realm.derlen) != 0) {
		return false;
	}
	if (session->client_domain [open_answer->service_realm.derlen] != '\0') {
		return false;
	}
	//
	// Found it -- clone the Session-Id into the diasasl_session
	session->_opaque.sessionid = NULL;
	dermem_strdup (session, open_answer->session_id, &session->_opaque.sessionid);
	//
	// Return success to have the session returned
	log_debug ("_ripple_process_open_finder() accepts %p with sessionid %p and client_domain '%s'", session, session->_opaque.sessionid, session->client_domain);
	return true;
}
//
static bool _ripple_process_authn_finder (ripple_ref *maybe, void *cbdata) {
	struct diasasl_session *session = (struct diasasl_session *) maybe;
	log_debug ("_ripple_process_authn_finder() tries %p with sessionid %p and client_domain '%s'", session, session->_opaque.sessionid, session->client_domain);
	//
	// Skip sessions that have no session_id
	assert (session->_opaque.sessionid != NULL);
	if (session->_opaque.sessionid == sessionid_not_yet_set) {
		return false;
	}
	//
	// Skip sessions with a session_id different from the authentication
	DER_OVLY_Quick_DiaSASL_DiaSASL_Authn_Answer *authn_answer = cbdata;
	log_debug ("_ripple_process_open_finder() is looking for session '%.*s'", (int)authn_answer->session_id.derlen, authn_answer->session_id.derptr);
	if (memcmp (authn_answer->session_id.derptr, session->_opaque.sessionid, authn_answer->session_id.derlen) != 0) {
		return false;
	}
	if (session->_opaque.sessionid [authn_answer->session_id.derlen] != '\0') {
		return false;
	}
	//
	// Found it -- return success to have the session returned
	log_debug ("_ripple_process_authn_finder() accepts %p with sessionid %p and client_domain '%s'", session, session->_opaque.sessionid, session->client_domain);
	return true;
}
//
int32_t diasasl_process (struct diasasl_node *node, bool just_one)
{
	SASL_DIAMETER_AAA diameter_aaa;
	void *tmpmem = NULL;
	if (!mem_open (0, &tmpmem)) {
		return ENOMEM;
	}
	int rc = 0;
	log_debug ("diasasl_process() reads from socket %d", node->socket);
	while (dermem_recv(tmpmem, node->socket, pack_diameter_aaa, (struct dercursor *) &diameter_aaa)) {
		log_debug("diasasl_process() dermem_recv: Succes");
		DER_OVLY_Quick_DiaSASL_DiaSASL_Open_Answer *open_answer = &diameter_aaa.open_answer;
		DER_OVLY_Quick_DiaSASL_DiaSASL_Authn_Answer *authn_answer = &diameter_aaa.authn_answer;
		//
		// Prepare to fill structures
		struct diasasl_session *session = NULL;
		char *opt_mechanism_list = NULL;
		uint8_t *s2c_token = NULL;
		uint32_t s2c_tokenlen = 0;
		dercursor *final_comerr = NULL;
		if (!der_isnull (&open_answer->session_id)) {
			//
			// This is a DiaSASL-Open-Answer
			// Find the diasasl_session that it answers to
			//
			session = (struct diasasl_session *) ripple_find (
					&node->_opaque.head,
					_ripple_process_open_finder,
					open_answer);
			except_if (agony, session == NULL);
			//
			// Harvest fields: final-comerr, sasl-mechanisms
			//
			final_comerr = &open_answer->final_comerr;
			//
			if (!der_isnull (&open_answer->sasl_mechanisms)) {
				log_debug("open_answer->sasl_mechanisms: %.*s", (int)open_answer->sasl_mechanisms.derlen, open_answer->sasl_mechanisms.derptr);
				dermem_strdup (tmpmem, open_answer->sasl_mechanisms, &opt_mechanism_list);
			}
		} else if (!der_isnull (&authn_answer->session_id)) {
			//
			// This is a DiaSASL-Authn-Answer
			// Find the diasasl_session that it answers to
			//
			session = (struct diasasl_session *) ripple_find (
					&node->_opaque.head,
					_ripple_process_authn_finder,
					authn_answer);
			except_if (agony, session == NULL);
			//
			// Harvest fields: final-comerr, client-userid, client-domain
			//
			final_comerr = &authn_answer->final_comerr;
			//
			if (!der_isnull (&authn_answer->client_userid)) {
				log_debug("authn_answer->client_userid: %.*s\n", (int)authn_answer->client_userid.derlen, authn_answer->client_userid.derptr);
				session->client_userid = NULL;
				dermem_strdup (session, authn_answer->client_userid, &session->client_userid);
			}
			//
			if (!der_isnull (&authn_answer->client_domain)) {
				log_debug("authn_answer->client_domain: %.*s\n", (int)authn_answer->client_domain.derlen, authn_answer->client_domain.derptr);
				session->client_domain = NULL;
				dermem_strdup (session, authn_answer->client_domain, &session->client_domain);
			}
			//
			// Harvest the S2C token
			//
			s2c_token    = authn_answer->sasl_token.derptr;
			s2c_tokenlen = authn_answer->sasl_token.derlen;
			log_debug ("s2c_token #%d at %p", s2c_tokenlen, s2c_token);
		} else {
			except_ifnot (agony, "neither DiaSASL-Open-Answer nor DiaSASL-Authn-Answer");
		}
		//
		// Unpack the final-comerr field from whichever answer
		int32_t final_comerr_value;
		const int32_t *final_comerr_ptr = NULL;
		log_debug ("final_comerr->derlen=%d, ->derptr=%p, !isnull=%d", (int)final_comerr->derlen, final_comerr->derptr, !der_isnull (final_comerr));
		if (!der_isnull(final_comerr)) {
			except_if (agony,
				der_get_int32 (*final_comerr, &final_comerr_value));
			final_comerr_ptr = &final_comerr_value;
			log_debug("final_comerr: %d", final_comerr_value);
		}
		//
		// Not all the variables may be filled, making all the difference
		log_debug ("diasasl_process() invoking callback");
		assert (session != NULL);
		assert (session->callback != NULL);
		session->callback(session, final_comerr_ptr, opt_mechanism_list, s2c_token, s2c_tokenlen);
		//
		// When a final-comerr was provided, auto-remove the session
		// and require no explicit DiaSASL-Close-Request is needed.
		// Note: This could be optimised by adding it to ripple_find()
		if (final_comerr_ptr != NULL) {
			session->_opaque.sessionid = NULL;
			ripple_remove (&node->_opaque.head, &session->_opaque.next);
		}
		//
		// Support at most a single loop run
		if (just_one) {
			rc = 0;
			goto retcode;
		}
		//
		// Recycle the temporary memory for response handling
		mem_recycle (0, tmpmem);
	}
	//
	// The loop ended after a failed dermem_recv()
	rc = errno;
	if ((rc == EAGAIN) || (rc == EWOULDBLOCK)) {
		rc = 0;
	}
	log_debug("dermem_recv() bailed out with error code %d", rc);
retcode:
	return rc;
	//
	// Disconnect in agony after a protocol error
agony:
	mem_close (&tmpmem);
	log_debug ("diasasl_process() bails out in agony");
	return EBADMSG;
}


void diasasl_send (struct diasasl_node *node,
			struct diasasl_session *session,
			char *opt_mechanism_choice,
			uint8_t *c2s_token, uint32_t c2s_token_len)
{
	assert (session->_opaque.sessionid != NULL);
	assert (session->_opaque.sessionid != sessionid_not_yet_set);
	log_debug ("DiaSASL-Authn-Request in session %s", session->_opaque.sessionid);
	//
	// Fill a structure for the AA-Request
	SASL_DIAMETER_AAR diameter_aar;
	memset (&diameter_aar, 0, sizeof (diameter_aar));
	if (opt_mechanism_choice != NULL) {
		diameter_aar.sasl_mechanism.derptr = (uint8_t *)opt_mechanism_choice ;
		diameter_aar.sasl_mechanism.derlen = strlen(opt_mechanism_choice);
	}
	diameter_aar.sasl_token.derptr = c2s_token;
	diameter_aar.sasl_token.derlen = c2s_token_len;
	diameter_aar.session_id.derptr = (uint8_t *)session->_opaque.sessionid ;
	diameter_aar.session_id.derlen = strlen(session->_opaque.sessionid);
	diameter_aar.sasl_channel_binding.derptr = session->chanbind;
	diameter_aar.sasl_channel_binding.derlen = session->chanbind_len;
	//
	// Logging and sending
	if (!der_isnull (&diameter_aar.sasl_mechanism)) {
		log_debug("diameter_aar.sasl_mechanism: %.*s", (int)diameter_aar.sasl_mechanism.derlen, diameter_aar.sasl_mechanism.derptr);
	}
	if (!der_isnull (&diameter_aar.sasl_channel_binding)) {
		log_debug("diameter_aar.sasl_channel_binding: %.*s", (int)diameter_aar.sasl_channel_binding.derlen, diameter_aar.sasl_channel_binding.derptr);
	}
	if (dermem_send (session, node->socket, pack_diameter_aar, (const dercursor *) &diameter_aar)) {
		log_debug("dermem_send: Succes");
	} else {
		log_debug("dermem_send: FAIL!");
	}
}


void diasasl_close (struct diasasl_node *node,
		struct diasasl_session *session)
{
	if (session->_opaque.sessionid != NULL) {
		//
		// It looks like no final-comerr was delivered yet;
		// We therefore need to formally close the session.
		//
		// Remove the session from the node's list
		ripple_remove (&node->_opaque.head, &session->_opaque.next);
		//
		// Send DiaSASL-Close-Request to the node (no -Answer)
		// but forego it when no sessionid has been defined yet
		if (session->_opaque.sessionid != sessionid_not_yet_set) {
			SASL_DIAMETER_CLOSE diameter_close;
			memset(&diameter_close, 0, sizeof(diameter_close));
			diameter_close.session_id.derptr = (uint8_t *)session->_opaque.sessionid ;
			diameter_close.session_id.derlen = strlen(session->_opaque.sessionid);
			dermem_send(session, node->socket, pack_diameter_close, (const dercursor *) &diameter_close);
		}
	}
	session->_opaque.sessionid = NULL;
	session->_opaque.state = NULL;
	session->_opaque.state_len = 0;
}


/* Disrupt each dasasl_sesssion for a diasasl_node, by sending
 * an error message to each.  This triggers the same reactions
 * in callbacks as a failed Diameter response would.
 *
 * This is a variation on ripple_dropwhile() because the callbacks
 * must be invoked on the second, removed element instead of the
 * head without a lock on the next element.
 */
static bool _ripple_cb2_break (ripple_ref *gonerptr, void *cbdata) {
        ripple_ref *goner = gonerptr->ref;
        gonerptr->ref = goner->ref;
        goner->ref = NULL;
	struct diasasl_session *session = (struct diasasl_session *) goner;
	session->callback (session, (int32_t *) cbdata, NULL, NULL, 0);
        return true;
}
//
void diasasl_break (struct diasasl_node *node, int32_t com_errno) {
	(void) ripple_iter (&node->_opaque.head, true, NULL, _ripple_cb2_break, &com_errno);
}
