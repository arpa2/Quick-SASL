# Finds the Cyrus SASL2 libraries
#
#  Cyrus-SASL2_FOUND          - Set to "cyrussasl2" if Cyrus SASL2 is found
#  Cyrus-SASL2_VERSION        - Version of the library found
#  Cyrus-SASL2_INCLUDE_DIRS   - Directories to include to get Cyrus SASL2 headers
#  Cyrus-SASL2_LIBRARIES      - Libraries to link against for Cyrus SASL2
#  Cyrus-SASL2_DEFINITIONS    - Definitions to add to the compiler
#
include(FeatureSummary)

set_package_properties(Cyrus-SASL2 PROPERTIES
	DESCRIPTION "Find Cyrus-SASL version 2 development goodies"
	URL "https://github.com/cyrusimap/cyrus-sasl"
)

if (${Cyrus-SASL2_FOUND})
	set (Cyrus-SASL2_QUIETLY TRUE)
endif()

find_package(PkgConfig)
if(PkgConfig_FOUND)
    pkg_search_module(Cyrus-SASL2 libsasl2)
endif()    

if(NOT "${Cyrus-SASL2_LIBRARIES}" STREQUAL "")
    set (Cyrus-SASL2_FOUND TRUE)
    set (SASL_SYSTEM "cyrussasl2")
endif()

# If we just got sasl2 -- suitable for -l but only if we also
# know the library dirs and ldflags -- then translate it to a full path.
if (Cyrus-SASL2_FOUND AND "${Cyrus-SASL2_LIBRARIES}" STREQUAL "sasl2")
    find_library(_cs2_LIBRARY
        NAMES "${Cyrus-SASL2_LIBRARIES}"
        HINTS ${Cyrus-SASL2_LIBRARY_DIRS}
        DOC "Cyrus-SASL libraries"
    )
    if(_cs2_LIBRARY)
        set(Cyrus-SASL2_LIBRARIES ${_cs2_LIBRARY})
    endif()
endif()

if (Cyrus-SASL2_FOUND)
	if (NOT Cyrus-SASL2_QUIETLY)
		message (STATUS "Found package Cyrus-SASL2 ${Cyrus-SASL2_VERSION}")
		message (STATUS "Compiler definitions for Cyrus-SASL2 are \"${Cyrus-SASL2_DEFINITIONS}\"")
		message (STATUS "Libraries for Cyrus-SASL2 linking are \"${Cyrus-SASL2_LIBRARIES}\"")
	endif ()
else()
	if (Cyrus-SASL2_FIND_REQUIRED)
		message (FATAL_ERROR "Could not find REQUIRED package Cyrus-SASL2")
	else()
		message (STATUS "Optional package Cyrus-SASL2 was not found")
	endif()
endif()


